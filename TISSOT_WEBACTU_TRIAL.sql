MERGE DBO.DATA_TISSOT_WEBACTU_TRIAL AS T
USING (SELECT 
	T2.OLD_MAIL_CODE AS MAIL_CODE,
	T4.ID AS CONTACT_CIAM_ID,
	T3.ID AS OFFER_TEMPLATE_CODE,
	T1.date_debut,
	T1.date_fin,
	T1.subscription_date AS DATE_SOUSCRIPTION,
	T1.id_trial,
	T1.cact
FROM tmp_tissot_webactu_trial T1
INNER JOIN USERS_TISSOT_EMAILS T2 ON T1.EMAIL=T2.MAIL
INNER JOIN ARTICLES_TISSOT_CATALOGUE T3 ON T1.PRODUCT=T3.OFFER_TEMPLATE
INNER JOIN USERS_TISSOT_LUMIO T4 ON T1.ID_CIAM = T4.NUM_CLIENT_CONTACT
WHERE OPTI_REJECTED = 0 ) AS S
ON (T.[id_trial] = S.[id_trial]) 
WHEN NOT MATCHED BY TARGET
	THEN 
		INSERT  ([CREATED_DT]
		   ,[MAIL_CODE]
		   ,[CONTACT_CIAM_ID]
		   ,[OFFER_TEMPLATE_CODE]
		   ,[DATE_DEBUT]
		   ,[DATE_FIN]
		   ,[DATE_SOUSCRIPTION]
		   ,[ID_TRIAL]
		   ,[CACT])
	 VALUES (GETDATE()
		   ,S.[MAIL_CODE]
		   ,S.[CONTACT_CIAM_ID]
		   ,S.[OFFER_TEMPLATE_CODE]
		   ,S.[DATE_DEBUT]
		   ,S.[DATE_FIN]
		   ,S.[DATE_SOUSCRIPTION]
		   ,S.[ID_TRIAL]
		   ,S.[CACT]) 
WHEN MATCHED
	THEN 
		UPDATE SET T.[MODIFIED_DT] = GETDATE()
				,T.MAIL_CODE=S.MAIL_CODE
			  ,T.[CONTACT_CIAM_ID] = S.[CONTACT_CIAM_ID]
			  ,T.[OFFER_TEMPLATE_CODE] = S.[OFFER_TEMPLATE_CODE]
			  ,T.[DATE_DEBUT] = S.[DATE_DEBUT]
			  ,T.[DATE_FIN] = S.[DATE_FIN]
			  ,T.[DATE_SOUSCRIPTION] = S.[DATE_SOUSCRIPTION]
			  ,T.[CACT] = S.[CACT];
