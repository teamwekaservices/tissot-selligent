AS
BEGIN
SET NOCOUNT ON;

SELECT 
	id as selligent_mail_id, 
	name
	
FROM mails with (nolock)
where MODIFIED_DT > DATEADD(dd,-365,getdate()) or created_dt > DATEADD(dd,-365,getdate())
END