AS
BEGIN
SET NOCOUNT ON;

-- reactivation des personnes sortant des dormeurs emails
update T1  
set dormeur_potentiel=null, dormeur_permanent=null, sortie_dormeur_permanent= getdate(),sortie_dormeur_potentiel=getdate()
-- 1310476
from users_tissot_emails T1 
where 
(dormeur_potentiel = 1 or dormeur_permanent = 1) -- 121365
and 
(
exists (select 1 from DATA_TISSOT_CIAM_NL TA WITH (nolock) where T1.MAIL_CODE=TA.MAIL_CODE and LAST_CONSENTEMENT_NL > dateadd(mm,-6,getdate()))  --231
or exists (select 1 from data_tissot_download TB WITH (nolock) where T1.MAIL_CODE=TB.EMAIL_ID and date_activite > dateadd(mm,-6,getdate())) --627 
or exists (select 1 from DATA_TISSOT_FACTURES_SAP TC WITH (nolock) where T1.MAIL_CODE=TC.EMAIL_ID and date_facture > dateadd(yy,-2,getdate())) -- 960 
or exists (select 1 from DATA_TISSOT_LYNX_ABO TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_PROCHAINE_FACTURATION > dateadd(yy,-2,getdate())) -- 963
or exists (select 1 from TAXONOMY_L227_T3 TE with (nolock) where TE.USERID=T1.ID AND CLICK_LASTDT > dateadd(mm,-6,getdate()) ) --1009
or exists (select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate())) -- 1021 
or exists (select 1 from DATA_TISSOT_LP_EXTERNE TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate())) --1095 
or exists (select 1 from DATA_TISSOT_LUMIO_CAPTATION_LEADS TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate())) -- 1098
or exists (select 1 from DATA_TISSOT_LUMIO_NL_SELLIGENT TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and NL_RH_ABO_DT > dateadd(mm,-6,getdate())) -- 1144
or exists (select 1 from ARTICLES_TISSOT_TRIAL TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_DEBUT > dateadd(mm,-6,getdate())) --1192
)


END