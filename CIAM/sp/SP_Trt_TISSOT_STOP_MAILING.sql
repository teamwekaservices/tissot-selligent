-- traitement des stops pubs sotckés dans le MDM 

-- le job est en full, on vide et on remplit 
TRUNCATE TABLE DATA_TISSOT_STOP_MAILING

-- insertion des correspondances avec la table du CIAM
INSERT INTO DATA_TISSOT_STOP_MAILING (MAIL_CODE,SIRET,NUM_CLIENT_CONTACT)
select T2.MAIL_CODE, T1.SIRET, T2.NUM_CLIENT_CONTACT from Tmp_Tissot_Ciam_STOP_MAILING T1
left join USERS_TISSOT_LUMIO T2 ON T1.SIRET=T2.SIRET; 

-- insertion des correspondances avec la table du TISSOT_COMPTE
INSERT INTO DATA_TISSOT_STOP_MAILING (SIRET,COMPTE_ID)
select T1.SIRET, T2.COMPTE_ID from Tmp_Tissot_Ciam_STOP_MAILING T1
left join Data_TISSOT_PM_InfoSpec T2 ON T1.SIRET=T2.SIRET