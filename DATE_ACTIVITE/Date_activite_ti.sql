AS
BEGIN
SET NOCOUNT ON;


/*Liste des critères 


OK : date de dernier clic (hors clic sur lien de désabo sur nos e-mailings, infocoll/alertes MAJ et e-newsletters)
OK : date de dernière facture(one shot, formation ou réabo)
OK : Date de fin d’abonnement
OK : date de dernier téléchargement/DI/Formulaire sur le site (tous formulaires dontwebcallback + inscription webinar et replay + abandon de panier)
OK date de dernier RDV effectué tél ou terrain
NON PAS TI date de dernière participation à une session de formation
OK date de dernier abonnement à une newsletter (peut être différent du formulaire notamment chez TI avec enregistrement sur un formulaire partenaire)
NON PAS TI date de dernière authentification sur le site internet (login, pas cookie)
 date intégration fichier
OK Date de dernière création de devis (dans Selligent)
OK Appels assimilés à une activité (cf fichier « Statuts appels retenus – Toutes sociétés »
(1)


*/


----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TI_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------


--- 
--- DATE DERNIER CLIC
---
-- check temp table
IF OBJECT_ID('dbo.TMP_TI_ACTIVITE_EMAIL', 'U') IS NOT NULL
BEGIN;
	DROP TABLE dbo.TMP_TI_ACTIVITE_EMAIL;
END;

-- création table temporaire pour stocker les clics des emails
CREATE TABLE dbo.TMP_TI_ACTIVITE_EMAIL (
	MAIL_CODE INT NOT NULL,
	CLICK_LASTDT DATE
);


-- on vide la table des activités 
TRUNCATE TABLE dbo.TMP_TI_ACTIVITE_EMAIL;
TRUNCATE TABLE DATA_TI_DATE_ACTIVITE;

-- insertion des dates de clics pour les contacts
INSERT INTO dbo.TMP_TI_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L34_T4 T1 WITH (NOLOCK)
INNER JOIN USERS_TI_CONTACT T2 WITH (NOLOCK) ON T1.USERID=T2.ID AND T2.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date


-- insertion des dates de clics pour les suspects
INSERT INTO dbo.TMP_TI_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L49_T4 T1 WITH (NOLOCK)
INNER JOIN USERS_TI_SUSPECT T2 WITH (NOLOCK) ON T1.USERID=T2.ID AND T2.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date



-- insertion des dates de clics pour les emails (users)
INSERT INTO dbo.TMP_TI_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,USERID FROM TAXONOMY_L156_T4 T1 WITH (NOLOCK)
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- creation d'index
CREATE INDEX IDX_MASTER_CODE ON dbo.TMP_TI_ACTIVITE_EMAIL (MAIL_CODE);
CREATE INDEX IDX_MASTER_CODE2 ON dbo.TMP_TI_ACTIVITE_EMAIL (MAIL_CODE,CLICK_LASTDT);


-- Merge dans le table DATA_TI_EMAILS de la MAX CLICK_LASTDT 
MERGE USERS_TI_EMAILS AS T
USING (SELECT MAX(CLICK_LASTDT) AS CLICK_LASTDT,MAIL_CODE FROM dbo.TMP_TI_ACTIVITE_EMAIL GROUP BY MAIL_CODE)  AS S
ON (T.ID = S.MAIL_CODE) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.CLICK_LASTDT=S.CLICK_LASTDT;
			
-- 18s 
-- vidage de la table DATA_TI_DATE_ACTIVITE et purge des index 
TRUNCATE TABLE DATA_TI_DATE_ACTIVITE;
DROP INDEX IF EXISTS
    IDX_LISTID_USERID_DATE_ACTIVITE ON DATA_TI_DATE_ACTIVITE,
    IDX_TI_DATE_ACTIVITE_LISTID ON DATA_TI_DATE_ACTIVITE,
    IDX_TI_DATE_ACTIVITE_LISTID_DATE ON DATA_TI_DATE_ACTIVITE,
    IDX_TI_DATE_ACTIVITE_LISTID_USER ON DATA_TI_DATE_ACTIVITE,
    IDX_TI_DATE_ACTIVITE_LISTID ON DATA_TI_DATE_ACTIVITE,
    IDX_TI_DATE_ACTIVITE_MAIL_CODE ON DATA_TI_DATE_ACTIVITE,
    IDX_TMP_ACT_LISTID ON DATA_TI_DATE_ACTIVITE,
    IDX_USERID_DATE_ACTIVITE ON DATA_TI_DATE_ACTIVITE;

			
-- insertion dans la table de date d'activité de la dernière date de clic par contact 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	34,
	T1.ID,
	1,
	MAX(CLICK_LASTDT),
	T1.MAIL_CODE
FROM USERS_TI_CONTACT T1 
INNER JOIN USERS_TI_EMAILS T2 ON T2.ID = T1.MAIL_CODE AND CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID,T1.MAIL_CODE;

-- insertion dans la table de date d'activité de la dernière date de clic par suspect
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	49,
	T1.ID,
	1,
	MAX(CLICK_LASTDT),
	T1.MAIL_CODE
FROM USERS_TI_SUSPECT T1 
INNER JOIN USERS_TI_EMAILS T2 ON T2.ID = T1.MAIL_CODE AND CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID,T1.MAIL_CODE;

-- insertion dans la table de date d'activité de la dernière date de clic par email
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT  
	156,
	T1.ID,
	1,
	MAX(CLICK_LASTDT),
	T1.MAIL_CODE
FROM USERS_TI_EMAILS T1 
GROUP BY T1.ID,T1.MAIL_CODE;
--11s

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER ABONNEMENT
--- uniquement pour les contacts, car pas de ABONNEMENTS pour les suspects
--- 

-- sélection des contacts dont le compte a un abonnement
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	34,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT) 
FROM USERS_TI_CONTACT T1
INNER JOIN DATA_TI_ABO T2 ON T1.COMPTE_ID = T2.COMPTE_ID
GROUP BY T1.ID;


-- 11s


--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE STATS WEB TI 
--- 
-- Aggragations des stats envoyés par le site web TI 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,TYPE_ACTIVITE,EMAIL_ID,DATE_ACTIVITE)
SELECT 156,12,MAIL_CODE, MAX(DATA_DT) FROM (SELECT MAIL_CODE,MAX(DOWNLOAD_LAST_DT) AS DATA_DT FROM DATA_TI_STATS_WEB WHERE DOWNLOAD_LAST_DT IS NOT NULL GROUP BY MAIL_CODE
UNION 
SELECT MAIL_CODE,MAX(CONNEXION_LAST_DT)  AS DATA_DT FROM DATA_TI_STATS_WEB WHERE CONNEXION_LAST_DT IS NOT NULL GROUP BY MAIL_CODE
UNION
SELECT MAIL_CODE,MAX(CREATED_USER_DT) AS DATA_DT FROM DATA_TI_STATS_WEB WHERE CREATED_USER_DT IS NOT NULL GROUP BY MAIL_CODE
UNION 
SELECT MAIL_CODE,MAX(ACTIVATION_COMPTE_DT) AS DATA_DT FROM DATA_TI_STATS_WEB WHERE ACTIVATION_COMPTE_DT IS NOT NULL GROUP BY MAIL_CODE) T1
GROUP BY MAIL_CODE

--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE ACTIVITE WEB
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	156,
	T1.MAIL_CODE,
	4,
	MAX(DATE_ACTIVITE) 
	,MAIL_CODE
FROM DATA_TI_DOWNLOAD T1 
GROUP BY T1.MAIL_CODE;

--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIER ENVOI EMT
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	156,
	T1.USERID,
	13,
	MAX(CREATED_DT) 
	,T1.USERID
FROM ACTION_TI_EMT T1 
GROUP BY T1.USERID;


----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TI_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------





----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TI_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE ACTIVITE COMMERCIALE
--- uniquement pour les contacts 
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	34,
	T1.ID,
	6,
	MAX(DATE_START) 
	,T1.MAIL_CODE
FROM USERS_TI_CONTACT T1
INNER JOIN DATA_TI_ACTIVITY T2 ON T1.ID = T2.CONTACT_ID
WHERE STATUS='held'
GROUP BY T1.ID,T1.MAIL_CODE;


-- 35s



--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIERE ABONNEMENT NEWSLETTER 
--- 
-- sélection des contacts ayant une inscription à une newsletter 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	34,
	T1.MAIL_CODE,
	5,
	MAX(DATE_INSCRIPTION) ,
	T1.MAIL_CODE
FROM DATA_TI_NL T1 
GROUP BY T1.MAIL_CODE;


--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DEVIS AFFAIRE ATHENA 
--- 
-- sélection des contacts ayant une ligne de commande (affaire dans Athéna) 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,COMPTE_ID)
SELECT 
	34,
	CONTACT_ID,
	7,
	MAX(DATE_COMMANDE),
	MAX(COMPTE_ID) 
FROM Data_TI_LIGNE_CDE
GROUP BY CONTACT_ID;

-- 39s

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER APPEL TMK  
--- 
-- sélection des comptes ayant selon la liste des statuts validés) 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT 
	34,
	CONTACT_ID,
	8,
	MAX(RELEASE_DATE)
FROM DATA_TI_PP_ACTIVITY_TMK
WHERE STATUS IN ('PAS_CONCERNE_ABONNEMENT','PREFERE_VERSION_PAPIER','AUTRE_REFUS','CREATION_TRIAL_15J','PAS_BESOIN','PAS_BUDGET','ACTION_COMMERCIALE','FACTURE_PERDUE','CONCURRENCE_DIRECTE','FILIALE_NON_DECISIONNAIRE','PEUT_ETRE_ESSAI_EN_COURS','AUTRE_REFUS_ARGUMENTES','ENVOI_TRIAL_REFUS_CONSULT_DUO','ABO_PERDU','PEUT_ETRE_INFO_EMAIL','INTERET_A_SUIVRE','CONCURRENCE_INDIRECTE','NON_DEJA_CLIENT','PAS_INTERESSE','NON_PAS_DE_BESOIN','RAPPEL','NON_PAS_DE_BUDGET','ENVOI_MAIL','DEJA_CONTACT_DR','ENVOI_MAIL_BDES','DEJA_INSCRIT','ACCORD_SUITE_MAILTRIAL','ACCORD_ACTION_DIFFERE','FACTURE_RECOUVREE','ACCORD_HORS_ACTION_DIFFERE','OUI_CONNECTE','OUI_RDV_TERRAIN','INSCRIPTION_LIGNE','OUI_RDV_TEL','RDV_RC','ABO_SAUVE_TACITE','RDV_COMMERCIAL','PROPOSITION','ACCORD_ACTION','ABO_SAUVE_ECHEANCE','RDV_TERRAIN','OUI_DEVIS_ENVOYE','ABO_SAUVE_SWITCH','CONNECTE_A_VERIFIER','ACCORD_HORS_ACTION','IAS')
GROUP BY CONTACT_ID;

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER Chargement fichier
--- 
-- sélection des contact en fonction de leur origine 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT 
	34,
	CONTACT_ID,
	9,
	MAX(DATA_ORIGINES_FICHIERS.ORIGINE_DT)
FROM DATA_TI_PP_ORIGINE
INNER JOIN DATA_ORIGINES_FICHIERS ON DATA_ORIGINES_FICHIERS.ORIGINE = DATA_TI_PP_ORIGINE.ORIGINE
GROUP BY CONTACT_ID;

--------------------------------------------------------------------------------------------------------------------------------------------


--- DATE CREATION 
--- 
-- sélection des contacts  
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	34,
	USERS_TI_CONTACT.ID,
	11,
	CREATED_DT,
	MAIL_CODE
FROM USERS_TI_CONTACT WITH (NOLOCK)
WHERE CREATED_DT IS NOT NULL AND CREATED_DT > DATEADD(year,-3,GETDATE()) ;

-- sélection des suspects
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	49,
	USERS_TI_SUSPECT.ID,
	11,
	CREATED_DT,
	MAIL_CODE
FROM USERS_TI_SUSPECT 
WHERE CREATED_DT IS NOT NULL AND CREATED_DT > DATEADD(year,-3,GETDATE()) ;


-- sélection des emails 
INSERT INTO DATA_TI_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,EMAIL_ID)
SELECT
	156,
	T1.ID,
	11,
	CREATED_DT,
	T1.ID 
FROM USERS_TI_EMAILS T1 
WHERE CREATED_DT IS NOT NULL AND CREATED_DT > DATEADD(year,-3,GETDATE()) ;


--41s
--------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TI_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------





----------------------------------------------------------------------------------------------------
------------------- 	DEBUT SP_DQ_TI_ACTIVITY_DT_MERGE_01		  ------------------------------
----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
-- MISE A JOUR des MAIL_CODE pour toutes les tables  


MERGE DATA_TI_DATE_ACTIVITE AS T
USING (
	SELECT T1.ID, T2.MAIL_CODE
	FROM DATA_TI_DATE_ACTIVITE T1 
	INNER JOIN USERS_TI_CONTACT T2 ON T1.USERID=T2.ID
	WHERE LISTID=34 AND T1.EMAIL_ID IS NULL
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.EMAIL_ID=S.MAIL_CODE;


MERGE DATA_TI_DATE_ACTIVITE AS T
USING (
	SELECT T1.ID, T2.MAIL_CODE
	FROM DATA_TI_DATE_ACTIVITE T1 
	INNER JOIN USERS_TI_SUSPECT T2 ON T1.USERID=T2.ID
	WHERE LISTID=49 AND T1.EMAIL_ID IS NULL
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.EMAIL_ID=S.MAIL_CODE;




-- INSERTION des dates dans les utilisateurs 

-- selection et insertion de la dernière date d'activité par contact
MERGE USERS_TI_CONTACT AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, USERID
FROM DATA_TI_DATE_ACTIVITE
WHERE LISTID=34
GROUP BY USERID)  AS S
ON (T.ID = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;

-- selection et insertion de la dernière date d'activité par suspect
MERGE USERS_TI_SUSPECT AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, USERID
FROM DATA_TI_DATE_ACTIVITE
WHERE LISTID=49
GROUP BY USERID)  AS S
ON (T.ID = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;
			
			
-- selection et insertion de la dernière date d'activité par emails
MERGE USERS_TI_EMAILS AS T
USING (SELECT MAX(DATE_ACTIVITE) AS DATE_ACTIVITE, EMAIL_ID AS USERID
FROM DATA_TI_DATE_ACTIVITE
WHERE EMAIL_ID IS NOT NULL
GROUP BY EMAIL_ID)  AS S
ON (T.MAIL_CODE = S.USERID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DATE_ACTIVITE;





--- alignement des dates d'activités
MERGE USERS_TI_EMAILS AS T
USING (
	SELECT USERS_TI_EMAILS.ID,MAX(USERS_TI_CONTACT.DERNIERE_ACTIVITE_DT) AS DERNIERE_ACTIVITE_DT
 	FROM USERS_TI_EMAILS
	INNER JOIN USERS_TI_CONTACT ON USERS_TI_CONTACT.MAIL_CODE = USERS_TI_EMAILS.MAIL_CODE AND USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT < USERS_TI_CONTACT.DERNIERE_ACTIVITE_DT
GROUP BY USERS_TI_EMAILS.ID
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;


MERGE USERS_TI_EMAILS AS T
USING (SELECT USERS_TI_EMAILS.ID,MAX(USERS_TI_SUSPECT.DERNIERE_ACTIVITE_DT) AS DERNIERE_ACTIVITE_DT
 FROM USERS_TI_EMAILS
INNER JOIN USERS_TI_SUSPECT ON USERS_TI_SUSPECT.MAIL_CODE = USERS_TI_EMAILS.MAIL_CODE AND USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT < USERS_TI_SUSPECT.DERNIERE_ACTIVITE_DT
GROUP BY USERS_TI_EMAILS.ID
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;


MERGE USERS_TI_CONTACT AS T
USING (
	SELECT USERS_TI_CONTACT.ID,USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT
 	FROM USERS_TI_CONTACT
	INNER JOIN USERS_TI_EMAILS ON USERS_TI_CONTACT.MAIL_CODE = USERS_TI_EMAILS.MAIL_CODE AND USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT> USERS_TI_CONTACT.DERNIERE_ACTIVITE_DT
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;



MERGE USERS_TI_SUSPECT AS T
USING (SELECT USERS_TI_SUSPECT.ID,USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT
 FROM USERS_TI_SUSPECT
INNER JOIN USERS_TI_EMAILS ON USERS_TI_SUSPECT.MAIL_CODE = USERS_TI_EMAILS.MAIL_CODE AND USERS_TI_EMAILS.DERNIERE_ACTIVITE_DT> USERS_TI_SUSPECT.DERNIERE_ACTIVITE_DT
)  AS S
ON (T.ID = S.ID) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.DERNIERE_ACTIVITE_DT=S.DERNIERE_ACTIVITE_DT;


-- toppage des contacts ayant 3 ans dépassés
UPDATE USERS_TI_CONTACT
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;


UPDATE USERS_TI_SUSPECT
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;

UPDATE USERS_TI_EMAILS
SET
	OPTOUT=999,
	OPTOUT_SOURCE='PURGE RGPD',
	OPTOUT_DT=GETDATE()
WHERE (DERNIERE_ACTIVITE_DT < DATEADD(year,-3,GETDATE()) OR DERNIERE_ACTIVITE_DT IS NULL) AND (OPTOUT <> 999 OR OPTOUT IS NULL) ;


-- réabilitation des personnes ayant un comportement alors qu'ils étaient en 999

UPDATE USERS_TI_CONTACT
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;

UPDATE USERS_TI_EMAILS
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;


UPDATE USERS_TI_SUSPECT
SET
	OPTOUT=NULL,
	OPTOUT_SOURCE=NULL,
	OPTOUT_DT=NULL
WHERE DERNIERE_ACTIVITE_DT > DATEADD(year,-3,GETDATE()) AND OPTOUT = 999;

