----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TISSOT_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER CLIC
--- La date de dernier clic est aggrégée au niveau de l'email unique
---
-- check temp table
IF OBJECT_ID('dbo.TMP_TISSOT_ACTIVITE_EMAIL', 'U') IS NOT NULL
BEGIN;
	DROP TABLE dbo.TMP_TISSOT_ACTIVITE_EMAIL;
END;

-- création table temporaire pour stocker les clics des emails
CREATE TABLE dbo.TMP_TISSOT_ACTIVITE_EMAIL (
	MAIL_CODE INT NOT NULL,
	CLICK_LASTDT DATE
);

-- insertion des dates de clics pour les contacts
INSERT INTO dbo.TMP_TISSOT_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L52_T3 WITH (NOLOCK)
INNER JOIN USERS_TISSOT_CONTACT WITH (NOLOCK) ON TAXONOMY_L52_T3.USERID=USERS_TISSOT_CONTACT.ID AND USERS_TISSOT_CONTACT.MAIL_CODE IS NOT NULL
WHERE CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les emails
INSERT INTO dbo.TMP_TISSOT_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT T1.CLICK_LASTDT,OLD_MAIL_CODE FROM TAXONOMY_L227_T3 T1 WITH (NOLOCK)
INNER JOIN USERS_TISSOT_EMAILS T2 WITH (NOLOCK) ON T1.USERID=T2.ID
WHERE T2.OLD_MAIL_CODE IS NOT NULL AND T1.CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les SAP
INSERT INTO dbo.TMP_TISSOT_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT T1.CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L227_T3 T1 WITH (NOLOCK)
INNER JOIN USERS_TISSOT_SAP T2 WITH (NOLOCK) ON T1.USERID=T2.ID
WHERE T2.MAIL_CODE IS NOT NULL AND  CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date

-- insertion des dates de clics pour les users CIAM
INSERT INTO dbo.TMP_TISSOT_ACTIVITE_EMAIL (CLICK_LASTDT,MAIL_CODE)
SELECT TAXONOMY_L241_T3.CLICK_LASTDT,MAIL_CODE FROM TAXONOMY_L241_T3 WITH (NOLOCK)
INNER JOIN USERS_TISSOT_LUMIO WITH (NOLOCK) ON TAXONOMY_L241_T3.USERID=USERS_TISSOT_LUMIO.ID
WHERE TAXONOMY_L241_T3.CLICK_LASTDT IS NOT NULL; -- pour init ne pas prendre le critère de date


-- creation d'index
CREATE INDEX IDX_MASTER_CODE ON TMP_TISSOT_ACTIVITE_EMAIL (MAIL_CODE);
CREATE INDEX IDX_MASTER_CODE2 ON TMP_TISSOT_ACTIVITE_EMAIL (MAIL_CODE,CLICK_LASTDT);
-- Merge dans le table DATA_TISSOT_EMAILS de la MAX CLICK_LASTDT 
MERGE USERS_TISSOT_EMAILS AS T
USING (SELECT MAX(CLICK_LASTDT) AS CLICK_LASTDT,MAIL_CODE FROM dbo.TMP_TISSOT_ACTIVITE_EMAIL GROUP BY MAIL_CODE)  AS S
ON (T.OLD_MAIL_CODE = S.MAIL_CODE) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.CLICK_LASTDT=S.CLICK_LASTDT;
			
			
-- vidage de la table DATA_TISSOT_DATE_ACTIVITE et purge des index 
TRUNCATE TABLE DATA_TISSOT_DATE_ACTIVITE;
DROP INDEX IF EXISTS
    IDX_LISTID_USERID_DATE_ACTIVITE ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TISSOT_DATE_ACTIVITE_LISTID ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TISSOT_DATE_ACTIVITE_LISTID_DATE ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TISSOT_DATE_ACTIVITE_LISTID_USER ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TISSOT_DATE_ACTIVITE_LISTID ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TISSOT_DATE_ACTIVITE_MAIL_CODE ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_TMP_ACT_LISTID ON DATA_TISSOT_DATE_ACTIVITE,
    IDX_USERID_DATE_ACTIVITE ON DATA_TISSOT_DATE_ACTIVITE;
    
----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TISSOT_ACTIVITY_DT_01 		  ------------------------------
----------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TISSOT_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------
-- insertion dans la table de date d'activité de la dernière date de clic par contact 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT  
	52,
	T1.ID,
	1,
	MAX(CLICK_LASTDT)
FROM USERS_TISSOT_CONTACT T1
INNER JOIN USERS_TISSOT_EMAILS T2 ON T2.OLD_MAIL_CODE = T1.MAIL_CODE AND CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID;

-- insertion dans la table de date d'activité de la dernière date de clic par email
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT  
	227,
	T1.ID,
	1,
	MAX(CLICK_LASTDT)
FROM USERS_TISSOT_EMAILS T1 
WHERE CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID;

-- insertion dans la table de date d'activité de la dernière date de clic par CIAM
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT  
	227,
	T1.ID,
	1,
	MAX(CLICK_LASTDT)
FROM USERS_TISSOT_LUMIO T1
INNER JOIN USERS_TISSOT_EMAILS T2 ON T2.OLD_MAIL_CODE = T1.MAIL_CODE AND CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID;

-- insertion dans la table de date d'activité de la dernière date de clic par SAP
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT  
	189,
	T1.ID,
	1,
	MAX(CLICK_LASTDT)
FROM USERS_TISSOT_SAP T1
INNER JOIN USERS_TISSOT_EMAILS T2 ON T2.OLD_MAIL_CODE = T1.MAIL_CODE AND CLICK_LASTDT IS NOT NULL
GROUP BY T1.ID;

--11s

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIERE FACTURE SAP 
--- uniquement pour les contacts et emails, car pas de facture pour les suspects
--- 

-- sélection des contacts dont le compte a une facture
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT 
	52,
	USERS_TISSOT_CONTACT.ID,
	2,
	MAX(DATE_FACTURE) 
FROM USERS_TISSOT_CONTACT 
INNER JOIN DATA_TISSOT_FACTURES_SAP ON USERS_TISSOT_CONTACT.COMPTE_ID = DATA_TISSOT_FACTURES_SAP.COMPTE_ID
GROUP BY USERS_TISSOT_CONTACT.ID;

INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT 
	227,
	T1.ID,
	2,
	MAX(DATE_FACTURE) 
FROM USERS_TISSOT_EMAILS T1
INNER JOIN DATA_TISSOT_FACTURES_SAP T2 ON T2.EMAIL_ID = T1.OLD_MAIL_CODE
GROUP BY T1.ID;
--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER ABONNEMENT SAP 
--- uniquement pour les contacts et emails, car pas de ABONNEMENTS pour les suspects
--- 

-- sélection des contacts dont le compte a un abonnement
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	52,
	USERS_TISSOT_CONTACT.ID,
	3,
	MAX(FIN_ABONNEMENT) 
FROM USERS_TISSOT_CONTACT 
INNER JOIN DATA_TISSOT_ABO ON USERS_TISSOT_CONTACT.COMPTE_ID = DATA_TISSOT_ABO.COMPTE_ID
GROUP BY USERS_TISSOT_CONTACT.ID;

-- sélection des contacts dont le compte a un abonnement
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT) 
FROM USERS_TISSOT_EMAILS T1
INNER JOIN DATA_TISSOT_ABO T2 ON T1.OLD_MAIL_CODE = T2.MAIL_CODE
GROUP BY T1.ID;


-- sélection des contacts dont le compte a un abonnement en tant qu'acheteur
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT) 
FROM USERS_TISSOT_EMAILS T1
INNER JOIN DATA_TISSOT_ABO T2 ON T1.OLD_MAIL_CODE = T2.MAIL_CODE_ABONNE
GROUP BY T1.ID;


-- sélection des contacts dont le compte a un abonnement en tant qu'utilisateur
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT) 
FROM USERS_TISSOT_EMAILS T1
INNER JOIN DATA_TISSOT_ABO T2 ON T1.OLD_MAIL_CODE = T2.MAIL_CODE_USER
GROUP BY T1.ID;
-- 11s


--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER ABONNEMENT ZUORA 
--- uniquement pour les contacts, ciam et emails, car pas de ABONNEMENTS pour les suspects
--- 

-- sélection des contacts dont ayant un abonnement Zuora (Lumio ou actupremium) 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,COMPTE_ID)
SELECT
	52,
	USERS_TISSOT_CONTACT.ID,
	3,
	MAX(FIN_ABONNEMENT),
	COMPTE_ID 
FROM USERS_TISSOT_CONTACT 
INNER JOIN DATA_TISSOT_LYNX_ABO ON USERS_TISSOT_CONTACT.MAIL_CODE = DATA_TISSOT_LYNX_ABO.MAIL_CODE
GROUP BY USERS_TISSOT_CONTACT.ID,COMPTE_ID;

INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	241,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT)
FROM USERS_TISSOT_LUMIO T1 
INNER JOIN DATA_TISSOT_LYNX_ABO T2 ON T1.MAIL_CODE = T2.MAIL_CODE
GROUP BY T1.ID;

INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	3,
	MAX(FIN_ABONNEMENT)
FROM USERS_TISSOT_EMAILS T1 
INNER JOIN DATA_TISSOT_LYNX_ABO T2 ON T1.OLD_MAIL_CODE = T2.MAIL_CODE
GROUP BY T1.ID;


-- 


--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER BDES  
--- uniquement pour les emails, 
--- 

INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	3,
	MAX(DATE_DER_CONNEXION)
FROM USERS_TISSOT_EMAILS T1 
INNER JOIN DATA_TISSOT_BDES T2 ON T1.OLD_MAIL_CODE = T2.EMAIL_ID
GROUP BY T1.ID;

----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TISSOT_ACTIVITY_DT_02 		  ------------------------------
----------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TISSOT_ACTIVITY_DT_03 		  ------------------------------
----------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE ACTIVITE WEB
--- 
-- sélection des emails ayant une activité web 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	4,
	MAX(DATE_ACTIVITE)
FROM USERS_TISSOT_EMAILS T1 
INNER JOIN DATA_TISSOT_DOWNLOAD T2 ON T1.OLD_MAIL_CODE = T2.EMAIL_ID
GROUP BY T1.ID;

-- sélection des emails ayant une activité web lumio
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	52,
	T1.ID,
	4,
	MAX(DATE_ACTIVITE)
FROM USERS_TISSOT_EMAILS T1 
INNER JOIN DATA_TISSOT_LYNX_WEB_ACTIVITIES T2 ON T1.OLD_MAIL_CODE = T2.MAIL_CODE
GROUP BY T1.ID;

-- sélection des emails ayant une inscription webinaire
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.MAIL_CODE,
	4,
	MAX(WEBINAIRE_DT)
FROM DATA_TISSOT_WEBINAIRES T1
GROUP BY T1.MAIL_CODE;


-- sélection des emails ayant un lead lumio
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.MAIL_CODE,
	4,
	MAX(CREATED_DT)
FROM DATA_TISSOT_LUMIO_CAPTATION_LEADS T1
GROUP BY T1.MAIL_CODE;
--------------------------------------------------------------------------------------------------------------------------------------------
--- 
--- DATE DERNIERE ACTIVITE COMMERCIALE
--- uniquement pour les contacts 
--- 
-- sélection des contacts ayant une activité web 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	52,
	USERS_TISSOT_CONTACT.ID,
	6,
	MAX(DATE_START) 
FROM USERS_TISSOT_CONTACT 
INNER JOIN DATA_TISSOT_ACTIVITY ON USERS_TISSOT_CONTACT.ID = DATA_TISSOT_ACTIVITY.CONTACT_ID
WHERE STATUS='held'
GROUP BY USERS_TISSOT_CONTACT.ID;


-- 35s



--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIERE ABONNEMENT NEWSLETTER 
--- 

-- sélection des emails ayant une NL TISSOT 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	5,
	MAX(DATE_INSCRIPTION) 
FROM USERS_TISSOT_EMAILS T1
INNER JOIN DATA_TISSOT_NL T2 ON T1.OLD_MAIL_CODE = T2.EMAIL_ID
GROUP BY T1.ID;

-- sélection des emails ayant une inscription à une newsletter LUMIO
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	5,
	MAX(DATE_INSCRIPTION) 
FROM USERS_TISSOT_EMAILS T1 
INNER JOIN DATA_TISSOT_LYNX_NL T2 ON T1.OLD_MAIL_CODE =T2.MAIL_CODE
GROUP BY T1.ID;


-- sélection des emails ayant une inscription à une newsletter LUMIO géré dans Selligent
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.MAIL_CODE,
	5,
	MAX(NL_RH_ABO_DT) 
FROM DATA_TISSOT_LUMIO_NL_SELLIGENT T1 
WHERE NL_RH_ABO_DT IS NOT NULL
GROUP BY T1.MAIL_CODE;



--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DEVIS AFFAIRE ATHENA 
--- 
-- sélection des contacts ayant une ligne de commande (affaire dans Athéna) 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,COMPTE_ID)
SELECT 
	52,
	CONTACT_ID,
	7,
	MAX(DATE_COMMANDE),
	MAX(COMPTE_ID) 
FROM Data_TISSOT_LIGNE_CDE
GROUP BY CONTACT_ID;

-- 39s

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER APPEL TMK  
--- 
-- sélection des comptes ayant selon la liste des statuts validés) 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,TYPE_ACTIVITE,DATE_ACTIVITE,COMPTE_ID)
SELECT 
	50,
	8,
	MAX(RELEASE_DATE), 
	COMPTE_ID
FROM DATA_TISSOT_PM_ACTIVITY_TMK
WHERE STATUS IN ('PAS_CONCERNE_ABONNEMENT','PREFERE_VERSION_PAPIER','AUTRE_REFUS','CREATION_TRIAL_15J','PAS_BESOIN','PAS_BUDGET','ACTION_COMMERCIALE','FACTURE_PERDUE','CONCURRENCE_DIRECTE','FILIALE_NON_DECISIONNAIRE','PEUT_ETRE_ESSAI_EN_COURS','AUTRE_REFUS_ARGUMENTES','ENVOI_TRIAL_REFUS_CONSULT_DUO','ABO_PERDU','PEUT_ETRE_INFO_EMAIL','INTERET_A_SUIVRE','CONCURRENCE_INDIRECTE','NON_DEJA_CLIENT','PAS_INTERESSE','NON_PAS_DE_BESOIN','RAPPEL','NON_PAS_DE_BUDGET','ENVOI_MAIL','DEJA_CONTACT_DR','ENVOI_MAIL_BDES','DEJA_INSCRIT','ACCORD_SUITE_MAILTRIAL','ACCORD_ACTION_DIFFERE','FACTURE_RECOUVREE','ACCORD_HORS_ACTION_DIFFERE','OUI_CONNECTE','OUI_RDV_TERRAIN','INSCRIPTION_LIGNE','OUI_RDV_TEL','RDV_RC','ABO_SAUVE_TACITE','RDV_COMMERCIAL','PROPOSITION','ACCORD_ACTION','ABO_SAUVE_ECHEANCE','RDV_TERRAIN','OUI_DEVIS_ENVOYE','ABO_SAUVE_SWITCH','CONNECTE_A_VERIFIER','ACCORD_HORS_ACTION','IAS')
GROUP BY COMPTE_ID;

--------------------------------------------------------------------------------------------------------------------------------------------

--- 
--- DATE DERNIER Chargement fichier
--- 
-- sélection des contact en fonction de leur origine 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT 
	52,
	CONTACT_ID,
	9,
	MAX(DATA_ORIGINES_FICHIERS.ORIGINE_DT)
FROM DATA_TISSOT_PP_ORIGINE
INNER JOIN DATA_ORIGINES_FICHIERS ON DATA_ORIGINES_FICHIERS.ORIGINE = DATA_TISSOT_PP_ORIGINE.ORIGINE
GROUP BY CONTACT_ID;


----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TISSOT_ACTIVITY_DT_03 		  ------------------------------
----------------------------------------------------------------------------------------------------



----------------------------------------------------------------------------------------------------
------------------- 		DEBUT SP_DQ_TISSOT_ACTIVITY_DT_04 		  ------------------------------
----------------------------------------------------------------------------------------------------
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE,COMPTE_ID)
SELECT
	52,
	USERS_TISSOT_CONTACT.ID,
	11,
	MIN(CREATED_DT),
	COMPTE_ID 
FROM USERS_TISSOT_CONTACT WITH (NOLOCK)
WHERE CREATED_DT IS NOT NULL
GROUP BY USERS_TISSOT_CONTACT.ID,COMPTE_ID ;

-- sélection des ciam 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	241,
	T1.ID,
	11,
	MIN(CREATED_DT) 
FROM USERS_TISSOT_LUMIO T1 
WHERE CREATED_DT IS NOT NULL
GROUP BY T1.ID;


-- sélection des emails 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	11,
	MIN(CREATED_DT) 
FROM USERS_TISSOT_EMAILS T1 
WHERE CREATED_DT IS NOT NULL
GROUP BY T1.ID;

-- sélection des sap 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T1.ID,
	11,
	MIN(CREATED_DT) 
FROM USERS_TISSOT_SAP T1 
WHERE CREATED_DT IS NOT NULL
GROUP BY T1.ID;


-- rajout des trials 
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T2.ID,
	12,
	T1.DATE_FIN 
FROM ARTICLES_TISSOT_TRIAL T1 
INNER JOIN USERS_TISSOT_EMAILS T2 ON T1.MAIL_CODE = T2.MAIL_CODE
WHERE DATE_FIN IS NOT NULL;


-- rajout des date de maj profil  
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T2.ID,
	13,
	T1.LAST_MAJ_PROFIL_DT 
FROM DATA_TISSOT_CIAM_STAT T1 
INNER JOIN USERS_TISSOT_EMAILS T2 ON T1.MAIL_CODE = T2.MAIL_CODE
WHERE LAST_MAJ_PROFIL_DT IS NOT NULL;


-- rajout de la dernière connexion profil  
INSERT INTO DATA_TISSOT_DATE_ACTIVITE (LISTID,USERID,TYPE_ACTIVITE,DATE_ACTIVITE)
SELECT
	227,
	T2.ID,
	13,
	T1.LAST_LOGIN_DT 
FROM DATA_TISSOT_CIAM_STAT T1 
INNER JOIN USERS_TISSOT_EMAILS T2 ON T1.MAIL_CODE = T2.MAIL_CODE
WHERE LAST_LOGIN_DT IS NOT NULL;



----------------------------------------------------------------------------------------------------
------------------- 		FIN SP_DQ_TISSOT_ACTIVITY_DT_04 		  ------------------------------
----------------------------------------------------------------------------------------------------
