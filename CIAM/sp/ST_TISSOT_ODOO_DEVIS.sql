AS
BEGIN
SET NOCOUNT ON;

-- mise à jour des ID pour les contenus ARTICLE THEME

	DECLARE @TISSOT_TEMP_DEVIS_CORRESPONDANCE TABLE(ID_DEVIS INT, ID_PRODUITS_CATALOGUE INT)

INSERT @TISSOT_TEMP_DEVIS_CORRESPONDANCE(ID_DEVIS, ID_PRODUITS_CATALOGUE)
SELECT TI_A.ID as id_devis_tmp, TI_B.ID as 'id_catalogue_tissot'
FROM tmp_tissot_odoo_devis AS TI_A
CROSS APPLY STRING_SPLIT (TI_A.PRODUITS , ',' ) AS S
INNER JOIN ARTICLES_TISSOT_CATALOGUE AS TI_B ON TI_B.OFFER_TEMPLATE = LEFT(REPLACE(S.VALUE,' ',''), CHARINDEX('_', REPLACE(S.VALUE,' ','') + '_') - 1);


MERGE DATA_TISSOT_ODOO_DEVIS AS T
	USING (
		SELECT 	T1.ID,
		T1.TISSOT_ODOO_DEVIS_ID,
		DEVIS_NUM,
		DEVIS_STAGE,
		DEVIS_TYPE,
        RAISON_COMMANDE,
		T2.PRODUITS,
        TOTAL_HT,
        TOTAL_TTC,
        NUM_COMMANDE_SAP,
        DATE_SAISIE_SAP,
        DATE_CREATION,
        DATE_VALIDITE,
        TOTAL_HT_AVANT_REMISE,
        F_TISSOT_ODOO_OPPORTUNITY,
        F_TISSOT_CIAM_UTILISATEUR,
        F_TISSOT_REFERENTIEL_ENTREP,
        F_TISSOT_BIZ_DEV
		FROM DBO.TMP_TISSOT_ODOO_DEVIS T1
	        LEFT JOIN (Select TMP.ID_DEVIS, STRING_AGG(TMP.ID_PRODUITS_CATALOGUE, ' | ') AS PRODUITS 
			from (Select DISTINCT ID_DEVIS, ID_PRODUITS_CATALOGUE FROM @TISSOT_TEMP_DEVIS_CORRESPONDANCE) as TMP
			GROUP BY TMP.ID_DEVIS) T2 ON T1.ID = T2.ID_DEVIS

		) AS S
	ON (T.ODOO_DEVIS_ID = S.TISSOT_ODOO_DEVIS_ID) 
	WHEN NOT MATCHED BY TARGET
		THEN 
		INSERT(
                CREATED_DT,
		        ODOO_DEVIS_ID,
                DEVIS_NUM,
                DEVIS_STAGE,
                DEVIS_TYPE,
                RAISON_COMMANDE,
                PRODUITS,
                TOTAL_HT,
                TOTAL_TTC,
                NUM_COMMANDE_SAP,
                DATE_SAISIE_SAP,
                DATE_CREATION,
                DATE_VALIDITE,
				TOTAL_HT_AVANT_REMISE,
                ODOO_OPPORTUNITY,
                F_TISSOT_CIAM_UTILISATEUR,
                REFERENTIEL_ENTREP,
                BIZ_DEV
		)
		VALUES(
            GETDATE(),
			S.TISSOT_ODOO_DEVIS_ID,
			S.DEVIS_NUM,
			S.DEVIS_STAGE,
			S.DEVIS_TYPE,
			S.RAISON_COMMANDE,
			S.PRODUITS,
			S.TOTAL_HT,
			S.TOTAL_TTC,
			S.NUM_COMMANDE_SAP,
			S.DATE_SAISIE_SAP,
			S.DATE_CREATION,
			S.DATE_VALIDITE,
			S.TOTAL_HT_AVANT_REMISE,
            S.F_TISSOT_ODOO_OPPORTUNITY,
            S.F_TISSOT_CIAM_UTILISATEUR,
            S.F_TISSOT_REFERENTIEL_ENTREP,
            S.F_TISSOT_BIZ_DEV) 
	WHEN MATCHED 
		THEN 
		UPDATE SET T.MODIFIED_DT = GETDATE(),
			T.ODOO_DEVIS_ID = S.TISSOT_ODOO_DEVIS_ID,
			T.DEVIS_NUM = S.DEVIS_NUM,
			T.DEVIS_STAGE = S.DEVIS_STAGE,
			T.DEVIS_TYPE = S.DEVIS_TYPE,
			T.RAISON_COMMANDE = S.RAISON_COMMANDE,
			T.PRODUITS = S.PRODUITS,
			T.TOTAL_HT = S.TOTAL_HT,
			T.TOTAL_TTC = S.TOTAL_TTC,
			T.NUM_COMMANDE_SAP = S.NUM_COMMANDE_SAP,
			T.DATE_SAISIE_SAP = S.DATE_SAISIE_SAP,
			T.DATE_CREATION = S.DATE_CREATION,
			T.DATE_VALIDITE=S.DATE_VALIDITE,
			T.TOTAL_HT_AVANT_REMISE=S.TOTAL_HT_AVANT_REMISE,
            T.ODOO_OPPORTUNITY = S.F_TISSOT_ODOO_OPPORTUNITY,
            T.F_TISSOT_CIAM_UTILISATEUR = S.F_TISSOT_CIAM_UTILISATEUR,
            T.REFERENTIEL_ENTREP = S.F_TISSOT_REFERENTIEL_ENTREP,
            T.BIZ_DEV = S.F_TISSOT_BIZ_DEV;

END