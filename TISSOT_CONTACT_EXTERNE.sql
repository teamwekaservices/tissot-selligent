MERGE USERS_TISSOT_CONTACT_EXTERNE  T
USING (SELECT DISTINCT
	USERS_TISSOT_CONTACT_EXTERNE.ID,
	DATA_PP_TITLE.ID AS TITLE_CODE,
	DATA_PP_DEPARTMENT.ID AS DEPARTMENT_CODE,
	DATA_TISSOT_EMAILS.ID AS MAIL_CODE,
	DATA_TISSOT_PM_IDENTIFIANT.COMPTE_ID AS COMPTE_ID
FROM USERS_TISSOT_CONTACT_EXTERNE
	LEFT JOIN DATA_PP_TITLE ON DATA_PP_TITLE.CODE = USERS_TISSOT_CONTACT_EXTERNE.TITLE
	LEFT JOIN DATA_PP_DEPARTMENT ON DATA_PP_DEPARTMENT.CODE = USERS_TISSOT_CONTACT_EXTERNE.DEPARTMENT
	LEFT JOIN DATA_TISSOT_EMAILS ON USERS_TISSOT_CONTACT_EXTERNE.MAIL = DATA_TISSOT_EMAILS.MAIL
	LEFT JOIN DATA_TISSOT_PM_IDENTIFIANT ON DATA_TISSOT_PM_IDENTIFIANT.ID_SUGAR = USERS_TISSOT_CONTACT_EXTERNE.ID_COMPTE) AS S
ON (T.[ID] = S.[ID]) 
WHEN MATCHED 
	THEN 
		UPDATE SET 
			T.[TITLE_CODE] = S.TITLE_CODE,
			T.DEPARTMENT_CODE=S.DEPARTMENT_CODE,
			T.MAIL_CODE = S.MAIL_CODE,
			T.COMPTE_ID=S.COMPTE_ID;
