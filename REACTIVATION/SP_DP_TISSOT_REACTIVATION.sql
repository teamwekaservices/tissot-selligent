AS
BEGIN
SET NOCOUNT ON;


--- script de création d'actions 
-- on insère dans une table temporaire tous les IDs des users concernés 
IF OBJECT_ID('dbo.TMP_TISSOT_EMAILS_INACTIFS', 'U') IS NOT NULL
BEGIN;
	DROP TABLE dbo.TMP_TISSOT_EMAILS_INACTIFS;
END;

-- création table temporaire pour stocker les clics des emails
CREATE TABLE dbo.TMP_TISSOT_EMAILS_INACTIFS (
	USERID INT);

-- cas 1 de ceux qui ont déjà reçu des emails  
INSERT INTO dbo.TMP_TISSOT_EMAILS_INACTIFS (USERID)
select ID from USERS_TISSOT_EMAILS T1 WITH (nolock) 
where master_email_mkg=1
and (dormeur_potentiel is null or dormeur_potentiel = 0)
and (created_dt < dateadd(mm,-6,getdate()) or created_dt is null)
and not exists (select 1 from DATA_TISSOT_CIAM_NL TA WITH (nolock) where T1.MAIL_CODE=TA.MAIL_CODE and LAST_CONSENTEMENT_NL > dateadd(mm,-6,getdate()))
and not exists (select 1 from data_tissot_download TB WITH (nolock) where T1.MAIL_CODE=TB.EMAIL_ID and date_activite > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_FACTURES_SAP TC WITH (nolock) where T1.MAIL_CODE=TC.EMAIL_ID and date_facture > dateadd(yy,-2,getdate()))
and not exists (select 1 from DATA_TISSOT_LYNX_ABO TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_PROCHAINE_FACTURATION > dateadd(yy,-2,getdate()))
and exists (select 1 from TAXONOMY_L227_T3 TE with (nolock) where TE.USERID=T1.ID AND MAILS_SENT > 10 and (MAILS_CLICKED IS NULL OR CLICK_LASTDT < dateadd(mm,-6,getdate()) ))
and not exists (select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LP_EXTERNE TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LUMIO_CAPTATION_LEADS TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LUMIO_NL_SELLIGENT TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and NL_RH_ABO_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from ARTICLES_TISSOT_TRIAL TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_DEBUT > dateadd(mm,-6,getdate()))


-- cas 2 de ceux qui n'ont jamais reçu d'email 
INSERT INTO dbo.TMP_TISSOT_EMAILS_INACTIFS (USERID)
select ID  from USERS_TISSOT_EMAILS T1 WITH (nolock) 
where master_email_mkg=1
and (dormeur_potentiel is null or dormeur_potentiel = 0)
and (created_dt < dateadd(mm,-6,getdate()) or created_dt is null)
and not exists (select 1 from DATA_TISSOT_CIAM_NL TA WITH (nolock) where T1.MAIL_CODE=TA.MAIL_CODE and LAST_CONSENTEMENT_NL > dateadd(mm,-6,getdate()))
and not exists (select 1 from data_tissot_download TB WITH (nolock) where T1.MAIL_CODE=TB.EMAIL_ID and date_activite > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_FACTURES_SAP TC WITH (nolock) where T1.MAIL_CODE=TC.EMAIL_ID and date_facture > dateadd(yy,-2,getdate()))
and not exists (select 1 from DATA_TISSOT_LYNX_ABO TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_PROCHAINE_FACTURATION > dateadd(yy,-2,getdate()))
and exists (select 1 from TAXONOMY_L227_T3 TE with (nolock) where TE.USERID=T1.ID AND MAILS_SENT =0)
and not exists (select 1 from data_tissot_pm_infospec TF with (nolock) where TF.ID = T1.COMPTE_ID AND T1.COMPTE_ID IS NOT NULL and (activity_code in (121,122,123) or account_type_code = 7 or account_type_code is null ) )
and not exists (select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LP_EXTERNE TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LUMIO_CAPTATION_LEADS TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and CREATED_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from DATA_TISSOT_LUMIO_NL_SELLIGENT TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and NL_RH_ABO_DT > dateadd(mm,-6,getdate()))
and not exists (select 1 from ARTICLES_TISSOT_TRIAL TD WITH (nolock) where T1.MAIL_CODE=TD.MAIL_CODE and DATE_DEBUT > dateadd(mm,-6,getdate()))

-- on met à jour ces users avec un dormeur_potentiel = 1 
UPDATE USERS_TISSOT_EMAILS 
SET MISE_DORMEUR_POTENTIEL=GETDATE(),DORMEUR_POTENTIEL=1
WHERE 
EXISTS (SELECT 1 FROM TMP_TISSOT_EMAILS_INACTIFS WHERE TMP_TISSOT_EMAILS_INACTIFS.USERID = USERS_TISSOT_EMAILS.ID);

--on créé des actions dans la table ACTION_TISSOT_INACTIFS avec le ACTIONCODE= INACTIFSEMAILS

INSERT INTO ACTION_TISSOT_INACTIFS (CREATED_DT,ACTIONCODE,USERID)
SELECT GETDATE(),'INACTIFSEMAILS', USERID FROM TMP_TISSOT_EMAILS_INACTIFS
where not exists (SELECT 1 from ACTION_TISSOT_INACTIFS with (nolock) where ACTION_TISSOT_INACTIFS.userid= TMP_TISSOT_EMAILS_INACTIFS.userid and ACTION_TISSOT_INACTIFS.CREATED_DT > dateadd(mm,-6,getdate()) )



END