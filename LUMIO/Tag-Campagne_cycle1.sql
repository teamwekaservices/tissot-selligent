MERGE DATA_TISSOT_EMAILS AS T
USING (SELECT MIN(DT) AS DT,MAIL_CODE FROM FLAGS WITH (NOLOCK)
INNER JOIN USERS_TISSOT_CONTACT WITH (NOLOCK) ON FLAGS.USERID=USERS_TISSOT_CONTACT.ID
WHERE CAMPAIGNID = 3225 AND PROBEID NOT IN(-1,-10,200) AND LISTID IN (52,73) AND DT > DATEADD(hh,-5,GETDATE())
GROUP BY MAIL_CODE)  AS S
ON (T.ID = S.MAIL_CODE AND T.LUMIO_CYCLE1 IS NULL) 
WHEN MATCHED
	THEN 
		UPDATE SET 
			T.LUMIO_CYCLE1=1
			,T.LUMIO_CYCLE1_CLICK_DT=S.DT;
