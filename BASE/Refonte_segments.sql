--initialisation de la table 
truncate table DATA_TISSOT_SEGMENTS_MKG; 
insert into DATA_TISSOT_SEGMENTS_MKG (MAIL_CODE,USERID)
SELECT DISTINCT MAIL_CODE,ID FROM USERS_TISSOT_EMAILS with (nolock)
where MAIL_CODE IS NOT NULL;


-- création d'une colonne en booléen false par défaut 
-- ALTER TABLE DATA_TISSOT_SEGMENTS_MKG add PROSPECTS BIT default 'FALSE';

-- ERP Récents 
UPDATE T1
	SET T1.ERP_RECENT = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_USER)
		and (
			(STATUT_ABONNEMENT_CODE in (1,3) and DEBUT_ABONNEMENT > dateadd(mm,-3,getdate()) )
			or
			(STATUT_ABONNEMENT_CODE in (2) and FIN_ABONNEMENT > dateadd(mm,-3,getdate()) )
		)
		
	)

or 
exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_ABONNE)
		and (
			(STATUT_ABONNEMENT_CODE in (1,3) and DEBUT_ABONNEMENT > dateadd(mm,-3,getdate()) )
			or
			(STATUT_ABONNEMENT_CODE in (2) and FIN_ABONNEMENT > dateadd(mm,-3,getdate()) )
		)
		
	)
or 
exists (
	select 1 from DATA_TISSOT_FACTURES_SAP T2 with (nolock) 
		where (T1.MAIL_CODE = T2.EMAIL_ID)
		and DATE_FACTURE > dateadd(dd,-7,getdate())
)
or 
exists
	(select 1 from DATA_TISSOT_LYNX_ABO T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE)
		and (
			(STATUT_ABONNEMENT in (1) and DEBUT_ABONNEMENT > dateadd(mm,-3,getdate()) )
			or
			(STATUT_ABONNEMENT in (0) and FIN_ABONNEMENT > dateadd(mm,-3,getdate()) )
		)
		
	)


-- TAG_ABO_ACTIF
UPDATE T1
	SET T1.TAG_ABO_ACTIF = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_USER)
		and FIN_ABONNEMENT >getdate() 		
	)
or 
exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_ABONNE)
		and FIN_ABONNEMENT >getdate() 		
	)
or
exists
	(select 1 from DATA_TISSOT_LYNX_ABO T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE)
		and STATUT_ABONNEMENT in (1) 
	)


-- TAG_ABO_INACTIF
UPDATE T1
	SET T1.TAG_ABO_INACTIF = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where 
(
exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_USER)
		and FIN_ABONNEMENT < getdate()
	)
or 
exists
	(select 1 from data_tissot_abo T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE_ABONNE)
		and FIN_ABONNEMENT < getdate()
	)
or 
exists
	(select 1 from DATA_TISSOT_LYNX_ABO T2 with (nolock) 
		where (T1.MAIL_CODE = T2.MAIL_CODE)
		and STATUT_ABONNEMENT in (0) 
	)
)
and T1.TAG_ABO_ACTIF = 0;

--TAG_ACHAT_SS
UPDATE T1
	SET T1.TAG_ACHAT_SS = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where 
(
exists
	(select 1 from DATA_TISSOT_FACTURES_SAP T2 with (nolock) 
		inner join ARTICLES_TISSOT_CATALOGUE T3 with (nolock) ON T2.OFFER_TEMPLATE_CODE=T3.ID AND T3.BUSINESS_MODEL_CODE = 2
		where (T1.MAIL_CODE = T2.EMAIL_ID)
	)
)

-- PROSPECTS
UPDATE T1
	SET T1.PROSPECTS = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where 
TAG_ABO_ACTIF <> 1 
AND 
TAG_ABO_INACTIF <> 1 
AND 
TAG_ACHAT_SS <> 1

-- FRANCE_METRO
--update 06/01/22 : suppression Webinaires 
UPDATE T1
	SET T1.FRANCE_METRO = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
NOT EXISTS (
	SELECT 1 FROM DATA_TISSOT_CIAM_ADRESSE T2 with (nolock)
	where T1.MAIL_CODE = T2.MAIL_CODE AND 
	(
	T2.ADDRESS_DEPARTMENT NOT IN ('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96')
	OR
	T2.PAYS NOT IN ('FR')
	)
);
-- AND 
-- NOT EXISTS (
-- 	SELECT 1 FROM DATA_TISSOT_WEBINAIRES T2 with (nolock)
-- 	where T1.MAIL_CODE = T2.MAIL_CODE AND PAYS NOT IN ('France')
-- )


-- PME_COMMUN
UPDATE T1
	SET T1.PME_COMMUN = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
NOT EXISTS (
	SELECT 1 FROM DATA_TISSOT_CIAM_INFOSPEC T2 with (nolock)
	where T1.MAIL_CODE = T2.MAIL_CODE AND 
	(
	T2.ACCOUNT_TYPE NOT IN (1,5,8)
	or T2.ACTIVITY NOT IN (1,3,23,42,48,51,52,53,56,58,61,62,68,73,103,107,108,111,117,118,119,120,121,122,123,128,132,139,142,143,153,155)
	or T2.APE_CODE_NIV5 LIKE '75%'
	or T2.APE_CODE_NIV5 LIKE '84%'
	)
)

-- RH -- partie 1 
UPDATE T1
	SET T1.RH = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE=31
-- #10229 : BDESE dans le CIAM )
--or 
--exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE=31
--)
--or 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (SERVICE=31 OR TACHES_RH=1)
)
--or 
--exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE=31
--)

--- RH Partie 2  
UPDATE T1
	SET T1.RH = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM not exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE is not null
-- #10229 : BDESE dans le CIAM )
-- and
--not exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE is not null
--)
-- and 
not exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (SERVICE is not null or MANDAT is not null)
)
--and 
--not exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND (DEPARTMENT_CODE is not null or MANDAT is not null)
--)
and 
--condition en OU 
(
	exists (
		select 1 from TAXONOMY_L227_T3 T2 with (nolock) 
		WHERE T1.USERID=T2.USERID
		AND (
			T2.TOP_CAT1 IN ('SUJ_DDT_FORMATION_ENTRETIEN_PRO','SUJDDT_CONG_ABSENCE_MALADIE','SUJDDT_CONTRAT_DE_TRAVAIL','SUJDDT_LICENCIEMENT','SUJDDT_RUPTURE_CONTRAT_TRAVAIL','SUJDDT_SANCTION_DISCIPLINE','SUJDDT_TELETRAVAIL','ACT_BTP','ACT_METALLURGIE')
			OR T2.TOP_CAT2 IN ('SUJ_DDT_FORMATION_ENTRETIEN_PRO','SUJDDT_CONG_ABSENCE_MALADIE','SUJDDT_CONTRAT_DE_TRAVAIL','SUJDDT_LICENCIEMENT','SUJDDT_RUPTURE_CONTRAT_TRAVAIL','SUJDDT_SANCTION_DISCIPLINE','SUJDDT_TELETRAVAIL','ACT_BTP','ACT_METALLURGIE')
			OR T2.TOP_CAT3 IN ('SUJ_DDT_FORMATION_ENTRETIEN_PRO','SUJDDT_CONG_ABSENCE_MALADIE','SUJDDT_CONTRAT_DE_TRAVAIL','SUJDDT_LICENCIEMENT','SUJDDT_RUPTURE_CONTRAT_TRAVAIL','SUJDDT_SANCTION_DISCIPLINE','SUJDDT_TELETRAVAIL','ACT_BTP','ACT_METALLURGIE')
			OR T2.TOP_CAT4 IN ('SUJ_DDT_FORMATION_ENTRETIEN_PRO','SUJDDT_CONG_ABSENCE_MALADIE','SUJDDT_CONTRAT_DE_TRAVAIL','SUJDDT_LICENCIEMENT','SUJDDT_RUPTURE_CONTRAT_TRAVAIL','SUJDDT_SANCTION_DISCIPLINE','SUJDDT_TELETRAVAIL','ACT_BTP','ACT_METALLURGIE')
			OR T2.TOP_CAT5 IN ('SUJ_DDT_FORMATION_ENTRETIEN_PRO','SUJDDT_CONG_ABSENCE_MALADIE','SUJDDT_CONTRAT_DE_TRAVAIL','SUJDDT_LICENCIEMENT','SUJDDT_RUPTURE_CONTRAT_TRAVAIL','SUJDDT_SANCTION_DISCIPLINE','SUJDDT_TELETRAVAIL','ACT_BTP','ACT_METALLURGIE')
		)

	)
-- 953
or
	exists (
		select 1 from DATA_TISSOT_WEBINAIRES T2 with (nolock)
		where T1.MAIL_CODE = T2.MAIL_CODE 
		and exists (
			select 1 from ARTICLES_TISSOT_WEBIKEO_WEBINAIRES T3 with (nolock)
			WHERE T2.WEBIKEO_ID_WEBINAIRE=T3.WEBIKEO_ID AND MATIERE_WEBINAIRE = 1 
		)
	)
-- 2053 
--or 
--	exists (
--		select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--		WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		AND SUJET LIKE '%social%'
--	)
--2671
or 
	exists (
		select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
		where T1.MAIL_CODE = T2.EMAIL_ID
		and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_CHAMP ='droit-du-travail' and (THEME_LABEL NOT IN ('Rémunération BTP','Sécurité et santé au travail BTP','Rémunération métallurgie','Sécurité et santé au travail métallurgie','Management','Rémunération','Sécurité et santé au travail') OR THEME_LABEL IS NULL) )
	)
-- rajout du 01/09/2022
or 
	exists (
		select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
		where T1.MAIL_CODE = T2.EMAIL_ID
		and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND LABEL LIKE 'BDES%' AND THEME_CHAMP IN ('droit-du-travail'))
	)

-- 10 272
or 
	exists (
		select 1 from DATA_TISSOT_CIAM_NL T2 with (nolock)
		where T1.MAIL_CODE = T2.MAIL_CODE
		and (
			T2.TISSOT_NL_PME=1
			OR
			T2.TISSOT_NL_BTP=1
			OR
			T2.TISSOT_NL_AIJE=1
			OR
			T2.TISSOT_NL_METALLURGIE=1
			OR
			T2.TISSOT_NL_ALERTES_MC=1
			OR
			T2.BDES_OPTIN_NL_INFOBDES=1
		)
	)
-- 54 629
)

-- DIRECTION
UPDATE T1
	SET T1.DIRECTION = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE=11
-- #10229 : BDESE dans le CIAM )
-- or 
-- exists (
-- 	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
-- 	WHERE T1.MAIL_CODE = T2.MAIL_CODE
-- 	AND DEPARTMENT_CODE=11
-- )
--or 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND SERVICE=11
)
--or 
--exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE=11
--)



-- AUTRE_SERVICE
UPDATE T1
	SET T1.AUTRE_SERVICE = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE in (1,2,7,12,13,14,27,35,36)
-- #10229 : BDESE dans le CIAM )
-- or 
-- exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE in (1,2,7,12,13,14,27,35,36)
--)
-- or 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND SERVICE in (1,2,7,12,13,14,27,35,36)
)
--or 
--exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE in (1,2,7,12,13,14,27,35,36)
--)

-- COMPTA PAIE
UPDATE T1
	SET T1.COMPTA_PAIE = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE=17
-- #10229 : BDESE dans le CIAM )
-- or 
-- exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE=17
--)
-- or 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (SERVICE=17 OR TACHES_PAIE=1 OR TACHES_COMPTABILITE=1)
)
--or 
--exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE=17
--)


--- COMPTA_PAIE Partie 2  
UPDATE T1
	SET T1.COMPTA_PAIE = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
-- #10229 : BDESE dans le CIAM not exists (
-- #10229 : BDESE dans le CIAM 	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
-- #10229 : BDESE dans le CIAM 	WHERE T1.MAIL_CODE = T2.EMAIL_ID
-- #10229 : BDESE dans le CIAM 	AND SERVICE is not null
-- #10229 : BDESE dans le CIAM )
--and
--not exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE is not null
--)
-- and 
not exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (SERVICE is not null or MANDAT is not null)
)
--and 
--not exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND (DEPARTMENT_CODE is not null or MANDAT is not null)
--)
--and 
--not exists (
--	select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE is not null
--)
and 
--condition en OU 
(
	exists (
		select 1 from TAXONOMY_L227_T3 T2 with (nolock) 
		WHERE T1.USERID=T2.USERID
		AND (
			T2.TOP_CAT1 IN ('SUJ_DDT_PAIE_SALAIRE')
			OR T2.TOP_CAT2 IN ('SUJ_DDT_PAIE_SALAIRE')
			OR T2.TOP_CAT3 IN ('SUJ_DDT_PAIE_SALAIRE')
			OR T2.TOP_CAT4 IN ('SUJ_DDT_PAIE_SALAIRE')
			OR T2.TOP_CAT5 IN ('SUJ_DDT_PAIE_SALAIRE')
		)

	)
--or 
--	exists (
--		select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--		WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		AND SUJET LIKE 'paie'
--	)
or 
	exists (
		select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
		where T1.MAIL_CODE = T2.EMAIL_ID
		and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_LABEL IN ('Rémunération BTP','Rémunération métallurgie','Rémunération'))
	)
or 
	exists (
		select 1 from DATA_TISSOT_CIAM_NL T2 with (nolock)
		where T1.MAIL_CODE = T2.MAIL_CODE
		and T2.TISSOT_NL_PAIE=1
	)
)


-- EXPERTCOMPTABLE
UPDATE T1
	SET T1.EXPERTCOMPTABLE = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
not exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (
		APE_CODE_NIV5 LIKE '75%' 
		OR APE_CODE_NIV5 LIKE '84%' 
		or ACTIVITY not in (1,3,23,42,48,52,53,56,58,61,62,68,73,103,107,108,111,117,118,119,120,121,122,123,128,132,139,142,143,153,155)
		or ACCOUNT_TYPE not in (1,5,8)
	)
)
--- 718218
and 
-- début des sous conditions : 
(
	-- raison sociale 
	-- infospec
	exists (
		select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
		WHERE T1.MAIL_CODE = T2.MAIL_CODE
		AND (
			ACTIVITY in (37)
			or TITLE in (235)
		)
	)
	-- 132435
	OR
	-- blocs avec des ET 
	(	
		
		(
			exists (
			select 1 from USERS_TISSOT_LUMIO T2  with (nolock) 
			WHERE T1.MAIL_CODE = T2.MAIL_CODE
			AND (T2.RAISON_SOCIALE1 LIKE '%AUDIT%' OR T2.RAISON_SOCIALE1 LIKE '%AVOCAT%' OR T2.RAISON_SOCIALE1 LIKE '%CABINET%' OR T2.RAISON_SOCIALE1 LIKE '%COMPTA%' OR T2.RAISON_SOCIALE1 LIKE '%FIDUC%')  AND T2.RAISON_SOCIALE1 NOT LIKE '%MEDICAL%' AND T2.RAISON_SOCIALE1 NOT LIKE '%DENTAIRE%'  AND T2.RAISON_SOCIALE1 NOT LIKE '%GYNECO%'  AND T2.RAISON_SOCIALE1 NOT LIKE '%RADIOLOGIE%' AND T2.RAISON_SOCIALE1 NOT LIKE '%IMMO%' AND T2.RAISON_SOCIALE1 NOT LIKE '%AUDITI%' AND T2.RAISON_SOCIALE1 NOT LIKE '%AUTOMO%' AND T2.RAISON_SOCIALE1 NOT LIKE '%ARCHI%' AND T2.RAISON_SOCIALE1 NOT LIKE 'CE %'	
		)
		and
		not exists (
			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
			WHERE T1.MAIL_CODE = T2.MAIL_CODE
			AND (APE_CODE_NIV5 is not null or APE_CODE_NIV5 <> '')
		)

		)
	-- 3581
	OR 
		not exists (
			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
			WHERE T1.MAIL_CODE = T2.MAIL_CODE
			AND ( 
				MANDAT IS NOT NULL 
				OR
				TITLE NOT IN (6,25,28,31,42,45,56,74,77,79,81,83,88,94,100,120,129,162,176,180,198,224,235,240,262,263,275,312,313,314)
			)
		)
--		AND 
--		not exists (
--			select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			AND (MANDAT is not null and MANDAT <> '')
--		)
		AND 
		exists (
			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
			WHERE T1.MAIL_CODE = T2.MAIL_CODE
			AND APE_CODE_NIV5 = '6920Z'
		)
	)
)

--------------- FIN SECTION 1 dans SP_Trt_TISSOT_CIAM_SEGMENTS 

--------------- DEBUT SECTION 2 dans  SP_Trt_TISSOT_CIAM_SEGMENT_PART2

-- IRP
-- partie  
UPDATE T1
	SET T1.IRP = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND MANDAT in ('ce_membre','ce_resp_commission','ce_secretaire','ce_secretaire_adjoint','ce_tresorier','ce_tresorier_adjoint','chsct_membre','chsct_secretaire','chsct_secretaire_adjoint','dp','dup_membre','dup_secretaire','dup_secretaire_adjoint','dup_tresorier','dup_tresorier_adjoint','cse_membre','cse_secretaire','cse_tresorier')
)
--OR 
--exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND MANDAT in ('ce_membre','ce_resp_commission','ce_secretaire','ce_secretaire_adjoint','ce_tresorier','ce_tresorier_adjoint','chsct_membre','chsct_secretaire','chsct_secretaire_adjoint','dp','dup_membre','dup_secretaire','dup_secretaire_adjoint','dup_tresorier','dup_tresorier_adjoint','cse_membre','cse_secretaire','cse_tresorier')
--)


-- partie 3 
UPDATE T1
	SET T1.IRP = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE  
 (
-- debut des conditions en OR 
	exists (
		select 1 from TAXONOMY_L227_T3 T2 with (nolock) 
			WHERE T1.USERID=T2.USERID
			AND (
				T2.TOP_CAT1 IN ('SUJIRP_CSE','SUJIRP_DEFENSE_DES_SALARIES','SUJ_IRP_SECURITE_SANTE_AU_TRAVAIL')
				OR T2.TOP_CAT2 IN ('SUJIRP_CSE','SUJIRP_DEFENSE_DES_SALARIES','SUJ_IRP_SECURITE_SANTE_AU_TRAVAIL')
				OR T2.TOP_CAT3 IN ('SUJIRP_CSE','SUJIRP_DEFENSE_DES_SALARIES','SUJ_IRP_SECURITE_SANTE_AU_TRAVAIL')
				OR T2.TOP_CAT4 IN ('SUJIRP_CSE','SUJIRP_DEFENSE_DES_SALARIES','SUJ_IRP_SECURITE_SANTE_AU_TRAVAIL')
				OR T2.TOP_CAT5 IN ('SUJIRP_CSE','SUJIRP_DEFENSE_DES_SALARIES','SUJ_IRP_SECURITE_SANTE_AU_TRAVAIL')
			)
	)
	OR
	exists (
		select 1 from DATA_TISSOT_WEBINAIRES T2 with (nolock)
		where T1.MAIL_CODE = T2.MAIL_CODE 
		and exists (
			select 1 from ARTICLES_TISSOT_WEBIKEO_WEBINAIRES T3 with (nolock)
			WHERE T2.WEBIKEO_ID_WEBINAIRE=T3.WEBIKEO_ID AND MATIERE_WEBINAIRE = 2 
		)

	)
	--OR 
	--exists (
	--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
	--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	--	AND (SUJET LIKE '%IRP%' OR SUJET LIKE '%CSE%')
	--)
	OR
	exists (
		select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
		where T1.MAIL_CODE = T2.EMAIL_ID
		and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_CHAMP IN ('representants-du-personnel-ce'))
	)
	OR
	exists (
		select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
		where T1.MAIL_CODE = T2.EMAIL_ID
		and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_CHAMP IN ('representants-du-personnel-ce') and LABEL LIKE 'BDES%')
	)
	OR 
	exists (
		select 1 from DATA_TISSOT_CIAM_NL T2 with (nolock)
		where T1.MAIL_CODE = T2.MAIL_CODE
		and (T2.TISSOT_NL_RP=1 or TISSOT_NL_AIJE_CSE=1)
	)
)

--and 
--not exists (
--	select 1 from DATA_TISSOT_BDES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.EMAIL_ID
--	AND SERVICE is not null
--)
-- and
-- not exists (
--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE is not null
--)
and 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
	WHERE T1.MAIL_CODE = T2.MAIL_CODE
	AND (ACCOUNT_TYPE is null or ACCOUNT_TYPE in (10,2,3,4))
)
--and 
--not exists (
--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND (DEPARTMENT_CODE is not null or MANDAT is not null)
--)
--and 
--not exists (
--	select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES T2  with (nolock) 
--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--	AND DEPARTMENT_CODE is not null
--)


-- partie 4 
UPDATE T1
	SET T1.IRP = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE  
	exists (select 1 from ARTICLES_TISSOT_TRIAL T2
		where T1.MAIL_CODE=T2.MAIL_CODE 
		AND DATE_FIN < DATEADD(mm,-3,getdate())
		and exists (
			select 1 from ARTICLES_TISSOT_CATALOGUE T3 
			WHERE T3.ID=T2.OFFER_TEMPLATE_CODE 
			AND T3.GAMME_CODE=5
		)
	)


--
--
---- PREVENTEUR 
--UPDATE T1
--	SET T1.PREVENTEUR = 1 
--from DATA_TISSOT_SEGMENTS_MKG T1 
--WHERE  
--(
--	(
--		not exists (
--			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			AND (MANDAT is not null AND MANDAT <> '')
--		)
--		--and 
--		-- not exists (
--		--	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--		--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		--	AND (MANDAT is not null  AND MANDAT <> '')
--		-- )
--		and 
--		exists (
--			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			AND ACCOUNT_TYPE in (10,2,3,4)
--		)
--	)
--	-- 6242
--	OR 
--	not exists (
--		select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
--		WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		AND ACCOUNT_TYPE not in (1,5,8)
--	)
--	--724853
--)
--
--
--AND 
--(
--	(
--		exists (
--			select 1 from DATA_TISSOT_BDES T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.EMAIL_ID
--			AND SERVICE in (15,32,33,34)
--		)
--		--or 
--		--exists (
--		--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--		--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		--	AND DEPARTMENT_CODE in (15,32,33,34)
--		--)
--		or 
--		exists (
--			select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			AND SERVICE in (15,32,33,34)
--		)
--		-- or 
--		-- exists (
--		-- 	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--		-- 	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		-- 	AND DEPARTMENT_CODE in (15,32,33,34)
--		-- )
--		or 
--		exists (
--			select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES T2  with (nolock) 
--			WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			AND DEPARTMENT_CODE in (15,32,33,34)
--		)
--	OR 
--		(
--			not exists (
--				select 1 from DATA_TISSOT_BDES T2  with (nolock) 
--				WHERE T1.MAIL_CODE = T2.EMAIL_ID
--				AND SERVICE is not null
--			)
--			--and
--			--not exists (
--			--	select 1 from DATA_TISSOT_WEBINAIRES T2  with (nolock) 
--			--	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			--	AND DEPARTMENT_CODE is not null
--			--)
--			and 
--			not exists (
--				select 1 from DATA_TISSOT_CIAM_INFOSPEC T2  with (nolock) 
--				WHERE T1.MAIL_CODE = T2.MAIL_CODE
--				AND (SERVICE is not null or MANDAT is not null)
--			)
--			-- and 
--			-- not exists (
--			-- 	select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--			-- 	WHERE T1.MAIL_CODE = T2.MAIL_CODE
--			-- 	AND (DEPARTMENT_CODE is not null or MANDAT is not null)
--			-- )
--			and 
--			not exists (
--				select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES T2  with (nolock) 
--				WHERE T1.MAIL_CODE = T2.MAIL_CODE
--				AND DEPARTMENT_CODE is not null
--			)
--		)
--		AND 
--		(
--			exists (
--				select 1 from TAXONOMY_L227_T3 T2 with (nolock) 
--				WHERE T1.USERID=T2.USERID
--				AND (
--					T2.TOP_CAT1 IN ('SUJSST_SECURITE_SANTE_AU_TRAVAIL')
--					OR T2.TOP_CAT2 IN ('SUJSST_SECURITE_SANTE_AU_TRAVAIL')
--					OR T2.TOP_CAT3 IN ('SUJSST_SECURITE_SANTE_AU_TRAVAIL')
--					OR T2.TOP_CAT4 IN ('SUJSST_SECURITE_SANTE_AU_TRAVAIL')
--					OR T2.TOP_CAT5 IN ('SUJSST_SECURITE_SANTE_AU_TRAVAIL')
--				)
--
--			)
--		OR
--			exists (
--				select 1 from DATA_TISSOT_WEBINAIRES T2 with (nolock)
--				where T1.MAIL_CODE = T2.MAIL_CODE 
--				and exists (
--					select 1 from ARTICLES_TISSOT_WEBIKEO_WEBINAIRES T3 with (nolock)
--					WHERE T2.WEBIKEO_ID_WEBINAIRE=T3.WEBIKEO_ID AND MATIERE_WEBINAIRE = 3 
--				)
--			)
--
--		-- or 
--		-- 	exists (
--		-- 		select 1 from DATA_TISSOT_LP_EXTERNE T2  with (nolock) 
--		-- 		WHERE T1.MAIL_CODE = T2.MAIL_CODE
--		-- 		AND (SUJET LIKE 'SST' OR SUJET LIKE 'RPS') 
--		-- 	)
--		or 
--			exists (
--				select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
--				where T1.MAIL_CODE = T2.EMAIL_ID
--				and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_CHAMP IN ('sante-securite'))
--			)
--		or 
--			exists (
--				select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
--				where T1.MAIL_CODE = T2.EMAIL_ID
--				and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_LABEL IN ('Sécurité et santé au travail BTP','Sécurité et santé au travail métallurgie','Sécurité et santé au travail (DDT)'))
--			)
--		or 
--			exists (
--				select 1 from DATA_TISSOT_CIAM_NL T2 with (nolock)
--				where T1.MAIL_CODE = T2.MAIL_CODE
--				and (T2.TISSOT_NL_ST=1 OR T2.TISSOT_NL_RPS=1)
--			)
--		)
--
--	)	
--)


-- MANAGER 
UPDATE T1
	SET T1.MANAGER = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE  
exists (
	select 1 from TAXONOMY_L227_T3 T2 with (nolock) 
	WHERE T1.USERID=T2.USERID
	AND (
		T2.TOP_CAT1 IN ('SUJ_DDT_MANAGEMENT')
		OR T2.TOP_CAT2 IN ('SUJ_DDT_MANAGEMENT')
		OR T2.TOP_CAT3 IN ('SUJ_DDT_MANAGEMENT')
		OR T2.TOP_CAT4 IN ('SUJ_DDT_MANAGEMENT')
		OR T2.TOP_CAT5 IN ('SUJ_DDT_MANAGEMENT')
	)

)
-- 1305
or 
exists (
	select 1 from DATA_TISSOT_DOWNLOAD T2 with (nolock)
	where T1.MAIL_CODE = T2.EMAIL_ID
	and exists(select 1 from ARTICLES_TISSOT_CONTENU_ACTU T3 with (nolock) where T3.CONTENU_ID=T2.ID_CONTENU AND THEME_LABEL IN ('Management'))
)
-- 1361
or 
exists (
	select 1 from DATA_TISSOT_CIAM_NL T2 with (nolock)
	where T1.MAIL_CODE = T2.MAIL_CODE
	and (T2.TISSOT_NL_MGMT=1)
)
-- 121071
or 
exists (
	select 1 from DATA_TISSOT_CIAM_INFOSPEC T2 with (nolock)
	where T1.MAIL_CODE = T2.MAIL_CODE
	and IS_MANAGER=1
)


-- DOM-TOM 
UPDATE T1
	SET T1.DOMTOM = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
EXISTS (
	SELECT 1 FROM DATA_TISSOT_CIAM_ADRESSE T2 with (nolock)
	where T1.MAIL_CODE = T2.MAIL_CODE AND 
	(
	T2.ADDRESS_DEPARTMENT  IN ('97','98')
	OR
	T2.PAYS  IN ('BL','GF','GP','MF','MQ','PF','PM','RE','WF','YT')
	)
)



-- SST
--- partie 1 
UPDATE T1
	SET T1.SST = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
WHERE 
EXISTS (SELECT 1 FROM DATA_TISSOT_SEGMENTS_MKG T2 WHERE T1.MAIL_CODE = T2.MAIL_CODE AND (TAG_ACHAT_SS=1 OR TAG_ABO_INACTIF=1 OR TAG_ABO_ACTIF=1 ))
AND
(
    EXISTS (SELECT 1 FROM DATA_TISSOT_ABO T3 WHERE (T1.MAIL_CODE = T3.MAIL_CODE_ABONNE)
        AND EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CATALOGUE T4 WHERE T4.ID = T3.OFFER_TEMPLATE_CODE AND GAMME_CODE =6)
    )
    OR 
    EXISTS (SELECT 1 FROM DATA_TISSOT_ABO T3 WHERE (T1.MAIL_CODE = T3.MAIL_CODE)
        AND EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CATALOGUE T4 WHERE T4.ID = T3.OFFER_TEMPLATE_CODE AND GAMME_CODE =6)
    )
    OR 
    EXISTS (SELECT 1 FROM DATA_TISSOT_ABO T3 WHERE (T1.MAIL_CODE = T3.MAIL_CODE_USER)
        AND EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CATALOGUE T4 WHERE T4.ID = T3.OFFER_TEMPLATE_CODE AND GAMME_CODE =6)
    )
    OR 
    EXISTS (SELECT 1 FROM DATA_TISSOT_FACTURES_SAP T4 WHERE (T1.MAIL_CODE = T4.EMAIL_ID)
        AND EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CATALOGUE T5 WHERE T5.ID = T4.OFFER_TEMPLATE_CODE AND GAMME_CODE =6)
    )
)


-- partie 2 
UPDATE T1
	SET T1.SST = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where not exists (select 1 from DATA_TISSOT_CIAM_INFOSPEC T2 WHERE T2.MAIL_CODE = T1.MAIL_CODE AND ACCOUNT_TYPE in (7))
and 
(
    exists (select 1 from DATA_TISSOT_CIAM_NL T3 WHERE T3.MAIL_CODE = T1.MAIL_CODE AND (TISSOT_NL_ST=1 OR TISSOT_NL_RPS=1) )
    OR 
    exists (select 1 from DATA_TISSOT_DOWNLOAD T4 WHERE T4.EMAIL_ID = T1.MAIL_CODE AND 
        EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CONTENU_ACTU T5 WHERE T5.CONTENU_ID = T4.ID_CONTENU AND THEME_CHAMP = 'sante-securite') 
    )
    OR 
    exists (select 1 from DATA_TISSOT_DOWNLOAD T4 WHERE T4.EMAIL_ID = T1.MAIL_CODE AND 
        EXISTS (SELECT 1 FROM ARTICLES_TISSOT_CONTENU_ACTU T5 WHERE T5.CONTENU_ID = T4.ID_CONTENU AND THEME_LABEL in('Sécurité et santé au travail BTP','Sécurité et santé au travail métallurgie','Sécurité et santé au travail')) 
    )
    OR 
    exists (select 1 from DATA_TISSOT_WEBINAIRES T6 WHERE T1.MAIL_CODE = T6.MAIL_CODE AND
        exists (select 1 from ARTICLES_TISSOT_WEBIKEO_WEBINAIRES T7 WHERE T7.WEBIKEO_ID = T6.WEBIKEO_ID_WEBINAIRE AND T7.MATIERE_WEBINAIRE=3)
    )
    OR 
    exists (select 1 from TAXONOMY_L227_T3 T8 WHERE T8.USERID=T1.USERID AND 
        (TOP_CAT1='SUJSST_SECURITE_SANTE_AU_TRAVAIL'
        OR TOP_CAT2='SUJSST_SECURITE_SANTE_AU_TRAVAIL'
        OR TOP_CAT3='SUJSST_SECURITE_SANTE_AU_TRAVAIL'
        OR TOP_CAT4='SUJSST_SECURITE_SANTE_AU_TRAVAIL'
        OR TOP_CAT5='SUJSST_SECURITE_SANTE_AU_TRAVAIL') 
    )
)

-- partie 3
UPDATE T1
	SET T1.SST = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where 
	exists (select 1 from ARTICLES_TISSOT_TRIAL T2
		where T1.MAIL_CODE=T2.MAIL_CODE 
		AND DATE_FIN < DATEADD(mm,-3,getdate())
		and exists (
			select 1 from ARTICLES_TISSOT_CATALOGUE T3 
			WHERE T3.ID=T2.OFFER_TEMPLATE_CODE 
			AND T3.GAMME_CODE=6
		)
	)
	or 
-- #10229 : BDESE dans le CIAM 	exists (select 1 from DATA_TISSOT_BDES T2 where  T1.MAIL_CODE=T2.EMAIL_ID AND SERVICE IN (15,32,33,34))
-- #10229 : BDESE dans le CIAM 	or 
	exists (select 1 from DATA_TISSOT_CIAM_INFOSPEC T2 where  T1.MAIL_CODE=T2.MAIL_CODE AND SERVICE IN (15,32,33,34))
--	or 
--	exists (select 1 from DATA_TISSOT_CAPTATION_LEADS_BDES T2 where  T1.MAIL_CODE=T2.MAIL_CODE AND DEPARTMENT_CODE IN (15,32,33,34))




-- ajout du 02/11/2023
-- RDV CALEDNLY
-- https://redmine.weka-ssc.fr/issues/11776
UPDATE T1
	SET T1.RDV_CALENDLY = 1 
from DATA_TISSOT_SEGMENTS_MKG T1 
where 
	exists (select 1 from DATA_TISSOT_CALENDLY T2
		where T1.MAIL_CODE=T2.MAIL_CODE 
		AND DATERDV > DATEADD(dd,-90,getdate())
	);

