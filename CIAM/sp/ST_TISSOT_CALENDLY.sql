AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: Author 
  -- DID - CreationDate: Creation Date 
  -- DID - Version: 0.1.0 
  -- DID - Description: Description 
  -- DID - Exceptions: Exceptions 
  -- DID - BusinessRules: Rule 
  -- DID - LastModifiedBy: LastModifiedBy 
  -- Update du 25/08/2023 : #11178, ajout de la donnée NOSHOW
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;

	MERGE DATA_TISSOT_CALENDLY AS T
	USING (
		SELECT 	T1.ID,
		IDEVENT,
		IDUSERREACH5,
		TYPE,
		SUJET,
		DATERDV,
		STATUTRDV,
		UTMCAMPAIGN,
		UTMSOURCE,
                UTMMEDIUM,
                UTMTERM,
                UTMCONTENT,
                T2.MAIL_CODE,
				NOSHOW
		FROM DBO.TMP_TISSOT_CALENDLY T1
	        LEFT JOIN USERS_TISSOT_LUMIO T2 ON T1.IDUSERREACH5 COLLATE SQL_Latin1_General_CP1_CS_AS = T2.NUM_CLIENT_CONTACT COLLATE SQL_Latin1_General_CP1_CS_AS

		) AS S
	ON (T.ID_EVENT = S.IDEVENT AND T.NUM_CLIENT_CONTACT = S.IDUSERREACH5 AND NULLIF(S.ID,'') IS NOT NULL) 
	WHEN NOT MATCHED BY TARGET
		THEN 
		INSERT(
                CREATED_DT,
		ID_EVENT,
                NUM_CLIENT_CONTACT,
                TYPE,
                SUJET,
                DATERDV,
                STATUSRDV,
                UTMCAMPAIGN,
                UTMSOURCE,
                UTMMEDIUM,
                UTMTERM,
                UTMCONTENT,
                MAIL_CODE,
				NOSHOW
		)
		VALUES(GETDATE(),
			S.IDEVENT,
			S.IDUSERREACH5,
			S.TYPE,
			S.SUJET,
			S.DATERDV,
			S.STATUTRDV,
			S.UTMCAMPAIGN,
			S.UTMSOURCE,
			S.UTMMEDIUM,
			S.UTMTERM,
			S.UTMCONTENT,
			S.MAIL_CODE,
			S.NOSHOW) 
	WHEN MATCHED 
		THEN 
		UPDATE SET T.MODIFIED_DT = GETDATE(),
			T.ID_EVENT = S.IDEVENT,
			T.NUM_CLIENT_CONTACT = S.IDUSERREACH5,
			T.TYPE = S.TYPE,
			T.SUJET = S.SUJET,
			T.DATERDV = S.DATERDV,
			T.STATUSRDV = S.STATUTRDV,
			T.UTMCAMPAIGN = S.UTMCAMPAIGN,
			T.UTMSOURCE = S.UTMSOURCE,
			T.UTMMEDIUM = S.UTMMEDIUM,
			T.UTMTERM = S.UTMTERM,
			T.UTMCONTENT = S.UTMCONTENT,
			T.MAIL_CODE=S.MAIL_CODE,
			T.NOSHOW=S.NOSHOW;

END