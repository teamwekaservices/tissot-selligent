AS
BEGIN
SET NOCOUNT ON;
---------------------------------- INSERTION DES CONTACTS vers DBO.ARTICLES_TISSOT_TRIAL ------------------------------------------------------------

--- REMPLACE le traitement ancien vers les trials Webactus
MERGE DBO.ARTICLES_TISSOT_TRIAL AS T
	USING (SELECT 
		 trial_id
		,f_ciam_id
		,date_debut
		,date_fin
		,USERS_TISSOT_LUMIO.ID AS USERID
		,cact
		,MAIL_CODE
		,IIF(T2.ID IS NULL, '415',T2.ID)  AS OFFER_TEMPLATE_CODE
		,CLI_CODE
		,DUREE
		,CODE_TA
		FROM Tmp_Tissot_Ciam_Trial T1
         INNER JOIN USERS_TISSOT_LUMIO ON T1.f_ciam_id COLLATE SQL_Latin1_General_CP1_CS_AS = USERS_TISSOT_LUMIO.NUM_CLIENT_CONTACT COLLATE SQL_Latin1_General_CP1_CS_AS
LEFT JOIN ARTICLES_TISSOT_CATALOGUE T2 ON T1.OFFER_TEMPLATE = T2.OFFER_TEMPLATE
	WHERE SIM_PARSELINE_RESULT=1	
	
	) AS S
	ON (T.ID_TRIAL = S.trial_id)
	WHEN NOT MATCHED BY TARGET
		THEN
			INSERT(
            ID_TRIAL
            ,CIAM_ID
            ,DATE_DEBUT
            ,DATE_FIN
            ,USERID
		    ,CACT
			,MAIL_CODE
			,CREATED_DT
			,OFFER_TEMPLATE_CODE
			,CLI_CODE
			,DUREE
			,CODE_TA)
			VALUES(
            S.trial_id
            ,S.f_ciam_id
            ,S.date_debut
            ,S.date_fin
		    ,S.USERID
		    ,S.CACT
			,S.MAIL_CODE
			,GETDATE()
			,S.OFFER_TEMPLATE_CODE
			,S.CLI_CODE
			,S.DUREE
			,S.CODE_TA)
	WHEN MATCHED
		THEN
			UPDATE SET
            T.DATE_DEBUT = S.date_debut
            ,T.DATE_FIN = S.date_fin
			,T.MAIL_CODE=S.MAIL_CODE
			,MODIFIED_DT=GETDATE()
			,T.OFFER_TEMPLATE_CODE=S.OFFER_TEMPLATE_CODE
			,T.CLI_CODE=S.CLI_CODE
			,T.DUREE=S.DUREE
			,T.CIAM_ID=S.f_ciam_id
			,T.CODE_TA=S.CODE_TA;
END