AS
BEGIN
SET NOCOUNT ON;
-- traitement des stops pubs sotckés dans le MDM 

-- le job est en full, on vide et on remplit 
TRUNCATE TABLE DATA_TISSOT_STOP_TEL

-- insertion des correspondances avec la table du CIAM
INSERT INTO DATA_TISSOT_STOP_TEL (MAIL_CODE,TEL_STANDARDISE,NUM_CLIENT_CONTACT)
select T2.MAIL_CODE, T1.TEL_STANDARDISE, T2.NUM_CLIENT_CONTACT from Tmp_Tissot_Ciam_STOP_TEL T1
left join USERS_TISSOT_LUMIO T2 ON T1.TEL_STANDARDISE=T2.TEL_STANDARD_NORMALISE; 

END