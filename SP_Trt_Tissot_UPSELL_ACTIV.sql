--ticket 6973 : gestion des upsell activité
@EMAIL NVARCHAR(250)	-- email du contact
, @NOM NVARCHAR(100)	-- email du contact
, @PRENOM NVARCHAR(100)	-- email du contact
, @SOCIETE NVARCHAR(100)	-- email du contact
, @TELEPHONE NVARCHAR(50)	-- email du contact
, @NUMERO_CLIENT_SAP NVARCHAR(100)	-- email du contact
, @ID_CIAM NVARCHAR(50)	-- email du contact
, @PRODUIT NVARCHAR(50)	-- email du contact
, @EMAIL_ID INT OUTPUT

 
AS
BEGIN
	

	SET NOCOUNT ON


		-- insertion dans la table DATA_TISSOT_EMAILS
		INSERT INTO DATA_TISSOT_EMAILS (MAIL,CREATED_DT,SOURCE,MAIL_DOMAIN) 
		SELECT @EMAIL,GETDATE(),9,SUBSTRING(@EMAIL,CHARINDEX('@',@EMAIL)+1,1000)
		WHERE NOT EXISTS (SELECT 1 FROM DATA_TISSOT_EMAILS WHERE DATA_TISSOT_EMAILS.MAIL=@EMAIL);
		
		-- insertion de l'utilisateur dans la table USERS_TISSOT_EMAILS
		INSERT INTO USERS_TISSOT_EMAILS (MAIL,CREATED_DT,MAIL_DOMAIN,OLD_MAIL_CODE,MAIL_CODE,SUBSCRIBE_SOURCE)
		SELECT MAIL,CREATED_DT,MAIL_DOMAIN,ID,ID,'FORM UPSELL ACTIV' FROM DATA_TISSOT_EMAILS WHERE MAIL = @EMAIL
		AND NOT EXISTS (SELECT 1 FROM USERS_TISSOT_EMAILS WHERE USERS_TISSOT_EMAILS.MAIL=@EMAIL);

--sélection du MAIL_CODE 
SELECT @EMAIL_ID=ID FROM USERS_TISSOT_EMAILS
WHERE USERS_TISSOT_EMAILS.MAIL = @EMAIL;

-- mise à niveau du profil user 
MERGE DATA_TISSOT_EMAILS_PROFIL AS T
USING (SELECT 
	@NOM AS NOM,
	@PRENOM AS PRENOM,
	@SOCIETE AS SOCIETE,
	@TELEPHONE AS TELEPHONE,
	@EMAIL_ID AS USERID,
	MAIL_CODE AS MAIL_CODE
	FROM USERS_TISSOT_EMAILS WITH (NOLOCK)
	WHERE ID=@EMAIL_ID ) AS S
ON (T.[USERID] = S.[USERID]) 
WHEN NOT MATCHED BY TARGET
	THEN 
		INSERT  ([USERID]
		   ,[NOM]
		   ,[PRENOM]
		   ,[SOCIETE]
		   ,[TELEPHONE]
		   ,MAIL_CODE
		   ,CREATED_DT)
	 VALUES (S.[USERID]
	    ,S.[NOM]
	    ,S.[PRENOM]
	    ,S.[SOCIETE]
	    ,S.[TELEPHONE]
	    ,S.MAIL_CODE
	    ,GETDATE()) 
WHEN MATCHED
	THEN 
		UPDATE SET T.[NOM] = S.[NOM]
			  ,T.[PRENOM] = S.[PRENOM]
			  ,T.[SOCIETE] = S.[SOCIETE]
			  ,T.[TELEPHONE] = S.[TELEPHONE]
			  ,T.[MAIL_CODE] = S.[MAIL_CODE]
			  ,T.[MODIFIED_DT] = GETDATE();

-- enregistrement du lead
		-- insertion de la réponse au formulaire
		INSERT INTO TISSOT_UPSELL_ACTIV (MAIL_CODE,CREATED_DT,NOM,PRENOM,SOCIETE,TELEPHONE,NUMERO_CLIENT_SAP,ID_CIAM,PRODUIT)
		SELECT ID,GETDATE(),LEFT(@NOM ,100),LEFT(@PRENOM,100),LEFT(@SOCIETE,100),LEFT(@TELEPHONE,50),LEFT(@NUMERO_CLIENT_SAP,14),LEFT(@ID_CIAM,50),LEFT(@PRODUIT,3) FROM DATA_TISSOT_EMAILS WHERE MAIL = @EMAIL; 


	
END