AS
BEGIN
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM
	-- INTERFERING WITH SELECT STATEMENTS.
	SET NOCOUNT ON;

	MERGE DBO.USERS_TISSOT_SAP AS T
	USING (SELECT NUM_CLIENT_CONTACT
			,RAISON_SOCIALE1
			,CIVILITE
			,NOM
			,PRENOM
			,ADRESSE_LIGNE1
			,ADRESSE_LIGNE2
			,ADRESSE_LIGNE3
			,CODE_POSTAL
			,VILLE
			,NUM_CLIENT_COMPTE
			,CASE 
				WHEN ADRESSE_EMAIL_STANDARDISE IS NULL THEN 'NR'
				ELSE ADRESSE_EMAIL_STANDARDISE
			END AS ADRESSE_EMAIL_STANDARDISE
			,CASE 
			 WHEN LEN(NUM_TELEPHONE_STANDARDISE) < 20 THEN NUM_TELEPHONE_STANDARDISE
			ELSE ''
			END AS NUM_TELEPHONE_STANDARDISE
			,SCORE_QUALITE_ADRESSE
			,PAYS
			,DATA_TISSOT_EMAILS.ID AS MAIL_CODE
	
		FROM tmp_sap_tissot 
		LEFT JOIN DATA_TISSOT_EMAILS ON DATA_TISSOT_EMAILS.MAIL = tmp_sap_tissot.ADRESSE_EMAIL_STANDARDISE
WHERE SIM_PARSELINE_RESULT = 1
		) AS S
	ON (T.NUM_CLIENT_CONTACT = S.NUM_CLIENT_CONTACT) 
	WHEN NOT MATCHED BY TARGET
		THEN 
			INSERT(MAIL
			,CREATED_DT
			,NUM_CLIENT_CONTACT
			,RAISON_SOCIALE1
			,CIVILITE
			,NOM
			,PRENOM
			,ADRESSE_LIGNE1
			,ADRESSE_LIGNE2
			,ADRESSE_LIGNE3
			,CODE_POSTAL
			,VILLE
			,NUM_CLIENT_COMPTE
			,ADRESSE_EMAIL_STANDARDISE
			,NUM_TELEPHONE_STANDARDISE
			,SCORE_QUALITE_ADRESSE
			,PAYS
			,MAIL_CODE)
			VALUES(S.ADRESSE_EMAIL_STANDARDISE
				,GETDATE()
				,S.NUM_CLIENT_CONTACT
				,S.RAISON_SOCIALE1
				,S.CIVILITE
				,S.NOM
				,S.PRENOM
				,S.ADRESSE_LIGNE1
				,S.ADRESSE_LIGNE2
				,S.ADRESSE_LIGNE3
				,S.CODE_POSTAL
				,S.VILLE
				,S.NUM_CLIENT_COMPTE
				,S.ADRESSE_EMAIL_STANDARDISE
				,S.NUM_TELEPHONE_STANDARDISE
				,S.SCORE_QUALITE_ADRESSE
				,S.PAYS
				,S.MAIL_CODE) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.MAIL=S.ADRESSE_EMAIL_STANDARDISE
				,T.MODIFIED_DT=GETDATE()
				,T.NUM_CLIENT_CONTACT=S.NUM_CLIENT_CONTACT
				,T.RAISON_SOCIALE1=S.RAISON_SOCIALE1
				,T.CIVILITE=S.CIVILITE
				,T.NOM=S.NOM
				,T.PRENOM=S.PRENOM
				,T.ADRESSE_LIGNE1=S.ADRESSE_LIGNE1
				,T.ADRESSE_LIGNE2=S.ADRESSE_LIGNE2
				,T.ADRESSE_LIGNE3=S.ADRESSE_LIGNE3
				,T.CODE_POSTAL=S.CODE_POSTAL
				,T.VILLE=S.VILLE
				,T.NUM_CLIENT_COMPTE=S.NUM_CLIENT_COMPTE
				,T.ADRESSE_EMAIL_STANDARDISE=S.ADRESSE_EMAIL_STANDARDISE
				,T.NUM_TELEPHONE_STANDARDISE=S.NUM_TELEPHONE_STANDARDISE
				,T.SCORE_QUALITE_ADRESSE=S.SCORE_QUALITE_ADRESSE
				,T.PAYS=S.PAYS
				,T.MAIL_CODE=S.MAIL_CODE;

UPDATE USERS_TISSOT_SAP SET FIRST_DATE_START_ABO = (SELECT MIN(DEBUT_ABONNEMENT) FROM DATA_TISSOT_ABO WHERE NUM_CLIENT_CONTACT=BP_CONTACT GROUP BY BP_CONTACT)
WHERE EXISTS(SELECT 1 FROM DATA_TISSOT_ABO WHERE NUM_CLIENT_CONTACT=BP_CONTACT GROUP BY BP_CONTACT);
				
END