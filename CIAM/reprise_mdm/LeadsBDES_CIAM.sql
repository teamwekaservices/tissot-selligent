-- utilisation d'une table stockant les données à importer 

-- sélection des données à insérer 
insert into weka_mdm.sa_tissot_activite_web (activite_id,b_loadid,b_classname,b_credate,b_creator,activite_type,activite_date,code_action,f_ciam_id,f_tissot_contenu_actu)
select  
nextval('weka_mdm.seq_tissot_activite_web') as activite_id, 
459847 as b_loadid,
'TissotActiviteWeb' as b_classname,
current_timestamp  as b_credate,
'loris' as b_creator,
'telechargement' as activite_type, 
activite_date as activite_date,
"CACT" as code_action,
"CIAMID" as f_ciam_id,
"CONTENU_ID" as f_tissot_contenu_actu 
from web_tissot.tissotimportleadsbdes


