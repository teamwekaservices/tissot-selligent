AS
BEGIN
SET NOCOUNT ON;
--Demande de Céline de base :
--	- Table Abo SAP  : 1 agrégat pour chaque produit suivant : REP, ST0, SE0, CSE, PB0 (abo_REP : si abo =1 sinon = vide)
--	- Table Abo SAP : ceux de moins de  3 mois et désabo de moins 3 mois (1 critère abo-desabo_recent =  Date de dernier abo et date de dernier désabo il y a moins de 3 mois)



-- calcul agrégats pour l'abonnement REP

--------------------------------------------DEUBUT_REP-------------------------------------------
DECLARE @time int = (select FORMAT(getdate(), 'HH'));

IF @time = 10
BEGIN

MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		MAIL_CODE
	FROM USERS_TISSOT_EMAILS T1 WITH (nolock)
	where
	exists (select 1 from DATA_TISSOT_ABO T2
			WHERE T1.MAIL_CODE=T2.MAIL_CODE
				AND OFFER_TEMPLATE LIKE '%REP%'
				AND STATUT_ABONNEMENT_CODE=1) ) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_REP)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_REP=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_REP=NULL;

--------------------------------------------FIN_REP----------------------------------------------
--------------------------------------------DEUBUT_STO-------------------------------------------

MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		MAIL_CODE
	FROM USERS_TISSOT_EMAILS T1 WITH (nolock)
	where
	exists (select 1 from DATA_TISSOT_ABO T2
			WHERE T1.MAIL_CODE=T2.MAIL_CODE
				AND OFFER_TEMPLATE LIKE '%ST0%'
				AND STATUT_ABONNEMENT_CODE=1) ) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_ST0)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_ST0=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_ST0=NULL;

--------------------------------------------FIN_STO----------------------------------------------
--------------------------------------------DEUBUT_SE0-------------------------------------------

MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		MAIL_CODE
	FROM USERS_TISSOT_EMAILS T1 WITH (nolock)
	where
	exists (select 1 from DATA_TISSOT_ABO T2
			WHERE T1.MAIL_CODE=T2.MAIL_CODE
				AND OFFER_TEMPLATE LIKE '%SE0%'
				AND STATUT_ABONNEMENT_CODE=1) ) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_SE0)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_SE0=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_SE0=NULL;

--------------------------------------------FIN_SE0----------------------------------------------
--------------------------------------------DEUBUT_CSE-------------------------------------------

MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		MAIL_CODE
	FROM USERS_TISSOT_EMAILS T1 WITH (nolock)
	where
	exists (select 1 from DATA_TISSOT_ABO T2
			WHERE T1.MAIL_CODE=T2.MAIL_CODE
				AND OFFER_TEMPLATE LIKE '%CSE%'
				AND STATUT_ABONNEMENT_CODE=1) ) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_CSE)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_CSE=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_CSE=NULL;

--------------------------------------------FIN_CSE----------------------------------------------
--------------------------------------------DEUBUT_PB0-------------------------------------------

MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		MAIL_CODE
	FROM USERS_TISSOT_EMAILS T1 WITH (nolock)
	where
	exists (select 1 from DATA_TISSOT_ABO T2
			WHERE T1.MAIL_CODE=T2.MAIL_CODE
				AND OFFER_TEMPLATE LIKE '%PB0%'
				AND STATUT_ABONNEMENT_CODE=1) ) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_PB0)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_PB0=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_PB0=NULL;


--------------------------------------------FIN_PB0----------------------------------------------
--------------------------------------------DEBUT_ABO RECENT-------------------------------------------
MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	select DISTINCT MAIL_CODE
FROM DATA_TISSOT_ABO
WHERE MAIL_CODE IS NOT NULL
AND (DEBUT_ABONNEMENT > DATEADD(month,-3,GETDATE()) OR FIN_ABONNEMENT BETWEEN DATEADD(month,-3,GETDATE()) AND GETDATE())) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,ABO_DESABO_RECENT)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.ABO_DESABO_RECENT=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.ABO_DESABO_RECENT=NULL;
--------------------------------------------FIN_ABO RECENT-------------------------------------------

-------------------------------------------- HORS_CIBLE -------------------------------------------
MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	select DISTINCT MAIL_CODE
	from users_tissot_emails T1 with (nolock)
	where
	--- exclusion des types de comptes (exclure particulier, EPIC et secteur public)
	-- de la base suspect
	exists (select 1 from USERS_TISSOT_SUSPECT TA with (nolock)  where T1.MAIL_CODE = TA.MAIL_CODE and TA.ACCOUNT_TYPE_CODE in (6,7,9))
	-- de la base compte
	or
	exists (select 1 from Data_TISSOT_PM_InfoSpec TA with (nolock)  where T1.COMPTE_ID = TA.COMPTE_ID and TA.ACCOUNT_TYPE_CODE in (6,7,9))
	) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,HORS_CIBLE)
		VALUES(S.MAIL_CODE,1)
WHEN MATCHED
	THEN
		UPDATE SET T.HORS_CIBLE=1
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.HORS_CIBLE=NULL;
-------------------------------------------- HORS_CIBLE -------------------------------------------


-------------------------------------------- FR -------------------------------------------
MERGE DATA_TISSOT_TEST_SITE_AGREGATS AS T
USING (
	SELECT
		T1.MAIL_CODE,
		CASE
			WHEN TA.PAYS = 'FR' OR TB.ADDRESS_COUNTRY = 'FR' THEN 1
			WHEN TA.PAYS <> 'FR' OR TB.ADDRESS_COUNTRY <> 'FR' THEN 0
	 		WHEN TA.PAYS IS NULL AND  TB.ADDRESS_COUNTRY IS NULL THEN NULL
		END AS FR
	from users_tissot_emails T1 with (nolock)
	LEFT JOIN USERS_TISSOT_LUMIO TA with (nolock)  on T1.MAIL_CODE = TA.MAIL_CODE
	LEFT JOIN DATA_TISSOT_SUS_InfoSpec TB ON TB.MAIL_CODE = T1.MAIL_CODE
	WHERE
	TA.PAYS IS NOT NULL OR TB.ADDRESS_COUNTRY IS NOT NULL

	) AS S
ON (T.MAIL_CODE = S.MAIL_CODE)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT (MAIL_CODE,FR)
		VALUES(S.MAIL_CODE,S.FR)
WHEN MATCHED
	THEN
		UPDATE SET T.FR=S.FR
WHEN NOT MATCHED BY SOURCE
	THEN
		UPDATE SET T.FR=NULL;
-------------------------------------------- FR -------------------------------------------


END
END

